declare module "axios" {
    const axios: any;
    export default axios;
}

declare module "strip" {
    function strip(HTMLString: string): string;
    export default strip;
}

declare module "js-cookie" {
    const Cookies: any;
    export default Cookies;
}
  
declare module "draftjs-to-html" {
    const drafToHTML: (rawContent: any) => string;
    export default drafToHTML;
}

declare module "html-to-draftjs" {
    const drafToHTML: (rawContent: string) => any;
    export default drafToHTML;
}
  