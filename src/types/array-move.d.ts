declare module "array-move" {
  export default function<T>(array: T[], fromIndex: number, toIndex: number): T[]
}


