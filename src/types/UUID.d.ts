declare module "lil-uuid" {
  namespace lil {
    function uuid(): string
    function isUUID(uuid: string): boolean
  }
  export = lil
}


