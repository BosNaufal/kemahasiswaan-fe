import * as React from 'react';
import styled from 'react-emotion'
// import { css } from 'emotion'
import ReactPagination from "react-js-pagination"

export interface PaginationProps {
  active: number
  perPage: number
  totalData: number
  onChange: (pageNumber: number) => void
}
 
const Pagination: React.SFC<PaginationProps> = ({
  active,
  perPage,
  totalData,
  onChange,
}) => {
  return (
    <PaginationWrapper>
      <ReactPagination
        hideNavigation={true}
        hideFirstLastPages={true}
        activePage={active}
        itemsCountPerPage={perPage}
        totalItemsCount={totalData}
        pageRangeDisplayed={5}
        onChange={onChange}
      />
    </PaginationWrapper>
  );
}

const PaginationWrapper = styled('div')`
  margin-top: 25px;

  .pagination {
    justify-content: center;
    display: flex;
    list-style: none;
    padding: 0;

    li {
      &.active > a {
        background: white;
        color: var(--primary);
      }
      & > a {
        box-shadow: 1px 2px 5px 0 rgba(0,0,0,.2);
        display: block;
        background: var(--primary);
        color: white;
        padding: 3px 15px;
        margin: 2px 5px;
        border-radius: 3px;
        text-decoration: none;
        font-size: 1.2em;
      }
    }
  }
`
 
export default Pagination;