import * as React from 'react';
import styled from 'react-emotion'

import Req from 'src/api'

import AsyncSelect from 'react-select/lib/Async';

interface AutocompleteProps {
  placeholder?: string
  isMulti?: boolean
  defaultValue?: any
  value?: any
  options?: any

  url?: string
  paramName?: string
  valueKey?: string
  labelKey?: string
  getValuePath?: any
  
  onDoneLoadOptions?: any
  onLoadOptions?: any
  onChange?: any
}
 
class Autocomplete extends React.Component<AutocompleteProps, any> {
  state = {
    defaultList: []
  }

  async componentDidMount() {
    const { onLoadOptions } = this.props
    let list = []
    if (onLoadOptions) {
      list = await onLoadOptions("")
    } else {
      list = await this.loadOptions("")
    }
    this.setState({ defaultList: list })
    return true
  }

  formatOptions = (options: any) => {
    const { valueKey, labelKey } = this.props
    return options.map((option: any) => ({ 
      ...option,
      value: option[valueKey!],
      label: option[labelKey!],
    }))
  }

  loadOptions = async (value: string) => {
    const { onDoneLoadOptions, paramName, url, getValuePath } = this.props
    const response = await Req(url, {
      params: { 
        [paramName!]: value,
      },
    })
    let options = response
    if (onDoneLoadOptions) {
      options = onDoneLoadOptions(response)
    }
    return this.formatOptions(getValuePath ? getValuePath(options) : options)
  }

  render() {
    const {
      placeholder = "Select...",
      isMulti = false,
      defaultValue,
      value,
      options = [],
  
      onLoadOptions,
      onChange,
    } = this.props

    return (
      <StyledAsyncSelect
        value={value}
        defaultValue={defaultValue}
        isMulti={isMulti}
        defaultOptions={this.state.defaultList || options}
        placeholder={placeholder}
        classNamePrefix="react-select"
        loadOptions={onLoadOptions || this.loadOptions}
        onChange={onChange}
      />
    )
  }
}

const StyledAsyncSelect = styled(AsyncSelect)`
  .react-select__control {
    box-shadow: none !important;
    
    &--is-focused {
      border: 1px solid var(--primary);
    }
  }
`
export default Autocomplete;
