import * as React from 'react';
import styled from 'react-emotion'

const Loading: React.SFC<{}> = () => (
  <Container>
    <Spinner>
      <div className="rect1" />
      <div className="rect2" />
      <div className="rect3" />
      <div className="rect4" />
      <div className="rect5" />
    </Spinner>
    <div>Please Wait...</div>
  </Container>
)

const Container = styled('div')`
  text-align: center;
  margin: 10px auto 15px;
  font-size: 1.1em;
  color: #888;
`

const Spinner = styled('div')`
  margin: 10px auto;
  width: 65px;
  height: 55px;
  text-align: center;
  font-size: 10px;

  & > div {
    background-color: var(--primary);
    height: 100%;
    width: 6px;
    display: inline-block;
    margin: 0 2px;
    
    -webkit-animation: sk-stretchdelay 1.2s infinite ease-in-out;
    animation: sk-stretchdelay 1.2s infinite ease-in-out;
  }

  .rect2 {
    -webkit-animation-delay: -1.1s;
    animation-delay: -1.1s;
  }

  .rect3 {
    -webkit-animation-delay: -1.0s;
    animation-delay: -1.0s;
  }

  .rect4 {
    -webkit-animation-delay: -0.9s;
    animation-delay: -0.9s;
  }

  .rect5 {
    -webkit-animation-delay: -0.8s;
    animation-delay: -0.8s;
  }

  @-webkit-keyframes sk-stretchdelay {
    0%, 40%, 100% { -webkit-transform: scaleY(0.4) }  
    20% { -webkit-transform: scaleY(1.0) }
  }

  @keyframes sk-stretchdelay {
    0%, 40%, 100% { 
      transform: scaleY(0.4);
      -webkit-transform: scaleY(0.4);
    }  20% { 
      transform: scaleY(1.0);
      -webkit-transform: scaleY(1.0);
    }
  }
`
 
export default Loading;