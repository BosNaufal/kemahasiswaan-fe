import * as React from 'react';
import dragula from 'dragula'
import 'dragula/dist/dragula.css'
import arrayMove from 'array-move';

export interface IDragulaContainerProps<K> {
  className?: string,
  style?: object
  items: K[]
  handle?: string
  children(item: K, index: number): React.ReactNode
  onSort(sortedItems: K[]): void
}
 
export type SortInfo = {
  direction: string,
  from: number,
  to: number
}
 
class DragulaContainer<K> extends React.Component<IDragulaContainerProps<K>, {}> {
  public rootEl: HTMLDivElement
  public drake: any
  public scroll: any

  public getIndex = (el: Element | null): number => {
    if (el == null) {
      return Infinity
    }
    const elDataIndex = el.getAttribute("data-index") as string
    const index = parseFloat(elDataIndex)
    return index
  }

  public getSortInfo = (el: Element, sibling: Element | null): SortInfo => {
    const { items } = this.props
    const fromIndex: number = this.getIndex(el)

    const sortInfo: SortInfo = {
      direction: "down",
      from: fromIndex,
      to: items.length,
    }

    if (sibling === null) {
      sortInfo.direction = "down"
      return sortInfo
    }

    const toIndex: number = this.getIndex(sibling)

    const DIRECTION: string = fromIndex < toIndex ? "down" : "up"
    const DEDUCTION = DIRECTION === "down" ? 1 : 0

    sortInfo.direction = DIRECTION
    sortInfo.to = toIndex - DEDUCTION

    return sortInfo
  }

  public handleDrop = (el: Element, target: Element, source: Element, sibling: Element): any => {
    const sortInfo: SortInfo = this.getSortInfo(el, sibling)
    const newItems = arrayMove(this.props.items, sortInfo.from, sortInfo.to)
    this.props.onSort([])
    this.setState({}, () => {
      this.props.onSort(newItems)
    })
  }

  public handleMove = (el: Element, source: Element, handleEl: Element) => {
    const { handle } = this.props
    const validHandle: Element | null = el.querySelector(`.${handle}`)
    if (!handle || !validHandle) {
      return true
    }
    const currentTargetIsHandle: boolean = validHandle === handleEl
    const currentTargetIsChildOfHandle: boolean = validHandle.contains(handleEl)
    return currentTargetIsHandle || currentTargetIsChildOfHandle
  }

  public dragulaSetup = () => {
    const me = this
    this.drake = dragula([this.rootEl], {
      moves: me.handleMove,
    })
    this.drake.on("drop", this.handleDrop)
  }

  public makeAutoScroll: EventListenerOrEventListenerObject = (e: MouseEvent) => {
    const { clientY } = e
    const MARGIN = 100
    const SPEED = 15

    // Horizontal Scroll
    const timeToScrollDown = clientY + MARGIN >= window.innerHeight 
    const timeToScrollUp = clientY <= MARGIN 
    const relativeParent = this.rootEl.offsetParent
    if (relativeParent && this.drake.dragging) {
      if (timeToScrollDown) {
        relativeParent.scrollTop += SPEED
      }
      if (timeToScrollUp) {
        relativeParent.scrollTop -= SPEED
      }
    }

  }

  public componentDidMount() {
    this.dragulaSetup()
    document.body.addEventListener('mousemove', this.makeAutoScroll, false)
  }

  public componentWillUnmount() {
    this.drake.destroy()
    document.body.removeEventListener('mousemove', this.makeAutoScroll, false)
  }

  public getContainerRef = (el: HTMLDivElement) => {
    this.rootEl = el
  }

  public render() { 
    const { items, children, className, style } = this.props
    return ( 
      <div 
        ref={this.getContainerRef} 
        className={className || ""}
        style={style || {}}
      >
        {items.map(children)}
      </div>
    );
  }
}
 
export default DragulaContainer;