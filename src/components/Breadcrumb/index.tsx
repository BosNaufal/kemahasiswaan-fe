import * as React from 'react';
import styled from 'react-emotion'


export type LinkType = {
  text: string,
  link?: string,
  active?: boolean
}

export interface BreadcrumbProps {
  links: LinkType[]
}
 
const Breadcrumb: React.SFC<BreadcrumbProps> = ({ links }: BreadcrumbProps) => {
  return (
    <Root>
    {links.map((link, index) => (
      <LinkGroup key={index}>
        <Link href={!link.active && link.link ? link.link : null} active={link.active}>{link.text}</Link>
        {index + 1 !== links.length &&
          <Separator>/</Separator>
        }
      </LinkGroup>
    ))}
    </Root>
  );
}
 
const Root = styled('div')`
  display: flex;
  background: #EEE;
  border-radius: 3px;
  padding: 3px 5px;
  margin-bottom: 15px;
`

const LinkGroup = styled('div')`
  display: flex;
  align-items: center;
`

const Link = styled('a')`
  padding: 3px 5px;
  margin-right: 5px;
  color: var(--primary);

  ${({ active }: any) => active && ({
    fontWeight: 700,
    textDecoration: 'none',
    color: 'black',
  })}
`

const Separator = styled('div')`
  font-size: 1.3em;
`
 
export default Breadcrumb;