import * as React from 'react';
import styled from 'react-emotion'

export interface NavigationProps {
  children: React.ReactNode;
  isLoading?: boolean
  loadingComponent?: React.ReactNode
}
 
const Navigation: React.SFC<NavigationProps> = ({
  children,
  isLoading,
  loadingComponent,
}: NavigationProps) => {
  return !isLoading ?
    <div>
      <MenuWrapper>
        {children}
      </MenuWrapper>
    </div>     
  : 
    <div>
      {!loadingComponent ?
        <div>Loading....</div>
      :
        loadingComponent
      }
    </div>
}

const MenuWrapper = styled('div')`
  display: flex;
  flex-wrap: wrap;
`
 
export default Navigation;
