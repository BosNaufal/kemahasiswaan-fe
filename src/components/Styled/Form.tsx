import * as React from 'react';
import { cx, css } from 'emotion'
import styled from 'react-emotion';


const BaseInputStyle = () => css`
  width: 100%;
  padding: 7px 10px;
  outline: 0;
  border: 1px solid #DDD;
  border-radius: 5px;

  &:focus {
    border-color: var(--primary);
  }
`
export const Input = styled("input")`
  ${BaseInputStyle}
`

export const Textarea = styled('textarea')`
  ${BaseInputStyle}
  min-height: 100px;
`

const SelectWrapper = styled("div")`
  border: 1px solid #DDD;
  position: relative;
  border-radius: 5px;

  &::after {
    top: 40%;
    right: 7px;
    content: "";
    position: absolute;
    border: 5px solid transparent;
    border-top-color: black;
  }
`

const SelectType = styled('select')`
  position: relative;
  width: 100%;
  outline: 0;
  background: none;
  display: block;
  appearance: none; 
  padding: 8px 10px;
  padding-right: 25px;
  border-radius: 0;
  border: 0;

  &:focus {
    border-color: var(--primary);
  }
`
export const Select = ({ className, style, ...rest }: React.SelectHTMLAttributes<HTMLSelectElement>) => (
  <SelectWrapper className={className} style={style}>
    <SelectType {...rest}>
      {rest.children}
    </SelectType>
  </SelectWrapper>
) 


export interface ICheckboxProps {
  className?: string
  style?: object
  type?: string
  color?: string
  label?: string
  round?: boolean,
  HTMLProps?: React.InputHTMLAttributes<HTMLInputElement>
}

const CheckboxWrapper = styled("div")`
  margin-right: 7px;

  & > .pretty .state label:before {
    border-color: #DDD;
  }

  &.pretty.p-default:not(.p-fill) input:checked ~ .state.p-success-o label:after {
    background-color: var(--primary)!important;
  }
  &.pretty input:checked ~ .state.p-success-o label:before, .pretty.p-toggle .state.p-success-o label:before {
    border-color: var(--primary);
  }
`

export const Checkbox = ({ 
  color = "success",
  round = false,
  className = "", 
  style = {}, 
  label = "",
  HTMLProps 
}: ICheckboxProps) => (
  <CheckboxWrapper style={style} className={cx("pretty p-default", className, { 'p-round': round })}>
    <input type="checkbox" {...HTMLProps} />
    <div className={cx("state", `p-${color}-o`)}>
      <label>{label}</label>
    </div>
  </CheckboxWrapper>
)