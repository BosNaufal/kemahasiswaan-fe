import styled from 'react-emotion';

export const Button = styled("button")`
  outline: none;
  margin: 5px 0;
  margin-right: 0;
  margin-left: 10px;
  padding: 7px 15px;
  font-size: 1em;

  background: var(--primary);
  color: white;

  ${({ danger = false }: any) => danger && ({
    background: 'var(--red)'
  })}

  ${({ disabled = false }: any) => disabled && ({
    background: '#888',
  })}

  /* color: var(--primary);
  background: var(--primary-light); */

  border: 0;

  ${({ inverse = false, active = false }: any) => (inverse || active) && ({
    background: 'white',
    color: 'var(--primary)',
  })}

  cursor: pointer;
  /* box-shadow: 1px 2px 5px 0 rgba(0,0,0,.3); */
  box-shadow: 1px 2px 5px 0 rgba(0,0,0,.2);
  display: flex;
  align-items: center;
  border-radius: 3px;

  i {
    font-size: 1.2em;
    margin-right: 10px;
  }
`