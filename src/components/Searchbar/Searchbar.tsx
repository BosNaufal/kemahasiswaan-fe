import * as React from 'react';
import styled from 'react-emotion'

import { Button } from 'src/components/Styled/General';
import { Input } from 'src/components/Styled/Form';

interface SearchbarProps {
  onSubmit: (query: string) => void
}

interface SearchbarState {
  query: string
}

class Searchbar extends React.PureComponent<SearchbarProps, SearchbarState> {
  state: SearchbarState = { 
    query: ""
  }

  handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.setState({ query: e.target.value })
  }

  clearInput = () => {
    this.setState({ query: "" }, () => {
      this.handleSubmit()
    })
  }
    
  handleSubmit = () => {
    this.props.onSubmit(this.state.query)
  }
  

  trackEnter = (e: React.KeyboardEvent) => {
    if (e.keyCode === 13) {
      e.preventDefault()
      this.handleSubmit()
    }
  }

  render() { 
    return (
      <InputWrapper>
        {this.state.query && 
          <StyledButton 
            onClick={this.clearInput}
            danger={true} 
            style={{ background: 'transparent', color: '#2b2b2b', boxShadow: 'none' }}
          >
            <i className="ion-md-close" />
          </StyledButton>
        }
        <StyledInput
          type="text" 
          placeholder="Search Here..." 
          value={this.state.query}
          onChange={this.handleInput}
          onKeyUp={this.trackEnter}
        />
        <StyledButton onClick={this.handleSubmit}>
          <i className="ion-md-search" style={{ marginRight: 7 }} />
          <span>SEARCH</span>
        </StyledButton>
      </InputWrapper>
    );
  }
}

const InputWrapper = styled('div')`
  display: flex;
  border: 1px solid #DDD;
  padding: 1px 2px;
  margin: 0 7px;
  margin-top: 7px;
`

const StyledInput = styled(Input)`
  border-radius: 0;
  border: 0;
`


const StyledButton = styled(Button)`
  margin: 0;
  i {
    margin-right: 0;
  }
`
 
export default Searchbar;