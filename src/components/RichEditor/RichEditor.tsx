import * as React from 'react';
import styled from 'react-emotion'
import { EditorState, convertToRaw, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';

import draftToHTML from 'draftjs-to-html';
import HTMLToDraft from 'html-to-draftjs';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

export interface IRichEditorProps {
  className?: string,
  style?: object
  fixedNav?: boolean,
  defaultValue?: string,
  onChange: (e: string) => void,
}

export interface IRichEditorState {
  editorState: EditorState
}
 
class RichEditor extends React.Component<IRichEditorProps, IRichEditorState> {
  public constructor(props: IRichEditorProps) {
    super(props);
    if (props.defaultValue) {
      const contentBlock = HTMLToDraft(props.defaultValue)
      if (contentBlock) {
        const contentState = ContentState.createFromBlockArray(contentBlock.contentBlocks);
        const editorState = EditorState.createWithContent(contentState);
        this.state = {
          editorState,
        }
      }
    } else {
      this.state = {
        editorState: EditorState.createEmpty(),
      };
    }

  }

  public onEditorStateChange = (editorState: EditorState): void => {
    this.setState({
      editorState,
    });
    const HTML = draftToHTML(convertToRaw(editorState.getCurrentContent()))
    this.props.onChange(HTML)
  };

  public handleUploadImage = (a: any) => {
    console.log(a)
  }

  public render() {
    const { editorState } = this.state;
    return (
      <EditorWrapper fixedNav={this.props.fixedNav}>
        <Editor
          toolbar={{
            image: {
              urlEnabled: false,
              uploadEnabled: true,
              uploadCallback: this.handleUploadImage
            }
          }}
          editorState={editorState}
          wrapperClassName="KMHS-wrapper"
          editorClassName="KMHS-editor"
          onEditorStateChange={this.onEditorStateChange}
        />
      </EditorWrapper>
    )
  }
}

const EditorWrapper = styled("div")`
  background: white;

  .KMHS-editor {
    padding: 0 10px;
    min-height: 450px;
    /* border: 1px solid #EEE; */
    margin-top: -5px;
    font-size: 1.1em;
    line-height: 1.5;
  }

  .KMHS-wrapper {
    .rdw-fontsize-wrapper, 
    .rdw-fontfamily-wrapper,
    .rdw-colorpicker-wrapper,
    .rdw-embedded-wrapper,
    .rdw-emoji-wrapper,
    .rdw-remove-wrapper,
    .rdw-option-wrapper[title='Monospace'],
    .rdw-option-wrapper[title='Superscript'],
    .rdw-option-wrapper[title='Subscript']
    {
      display: none;
    }
    
    ${({ fixedNav }: any) => fixedNav && `
      .rdw-editor-toolbar {
        position: absolute;
        width: 100%;
        background: white;
        z-index: 99;
        top: 81px;
        left: auto;
        max-width: 740px;
      }
    `}
  }
`
 
export default RichEditor;