import * as React from 'react';

import RichEditor from './RichEditor'

export interface IEditorProps {
  affixOffset: number
  defaultValue?: string
  onChange(html: string): void
}

export interface IEditorState {
  fixedEditorNav: boolean
}

class Editor extends React.Component<IEditorProps, IEditorState> {
  state = {
    fixedEditorNav: false,
  }

  mainContentEl: HTMLElement

  componentDidMount() {
    const mainContentEl = document.querySelector('.MainContent')
    if (mainContentEl) {
      this.mainContentEl = mainContentEl as HTMLElement
      this.mainContentEl.style.position = "inherit"
      this.mainContentEl.addEventListener('scroll', this.trackFixedRichNav, false)
    }
  }

  componentWillUnmount() {
    if (this.mainContentEl) {
      this.mainContentEl.style.position = "relative"
      this.mainContentEl.removeEventListener('scroll', this.trackFixedRichNav, false)
    }
  }

  trackFixedRichNav = () => {
    const affixOffset = this.props.affixOffset || 120
    if (this.mainContentEl.scrollTop > affixOffset) {
      this.setState({ fixedEditorNav: true })
    }
    if (this.mainContentEl.scrollTop < affixOffset) {
      this.setState({ fixedEditorNav: false })
    }
  }

  public render() { 
    return (
      <RichEditor 
        defaultValue={this.props.defaultValue}
        fixedNav={this.state.fixedEditorNav} 
        onChange={this.props.onChange}
      />
    );
  }
}
 
export default Editor;