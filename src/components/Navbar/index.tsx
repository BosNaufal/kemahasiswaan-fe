import * as React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom'

import MainNavbar from './MainNavbar'
import FlatNavbar from './FlatNavbar'

import store from './ViewModel';
import { observer } from 'mobx-react';

class Navbar extends React.Component<RouteComponentProps, {}> {
  public render() { 
    return store.show ? ( 
      store.type === "main" 
        ? <MainNavbar />
        : <FlatNavbar />
    ) : null;
  }
}

export default withRouter(observer(Navbar));
