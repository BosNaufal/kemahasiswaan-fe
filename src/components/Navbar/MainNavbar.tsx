import * as React from 'react';
import styled from 'react-emotion'
import { NavLink } from 'react-router-dom'
import { observer } from 'mobx-react';

import globalStore from 'src/pages/App/store'

class MainNavbar extends React.Component<{}, {}> {
  handleLogout = () => {
    globalStore.logout()
    window.location.reload()
  }
  
  public render() { 
    return ( 
      <Wrapper>
        <Inner className="mini-container">
          <Title>Beasiswa App</Title>
          <Menubar>
            <Menu to={'/'} exact={true}>Home</Menu>
            {globalStore.isLogged ?
              <React.Fragment>
                <Menu to={'/beasiswa/create'}>Create</Menu>
                <Menu to={'/analytic'}>Analytic</Menu>
                <Menu to={"#"} onClick={this.handleLogout}>Logout</Menu>
              </React.Fragment>
            :
              <Menu to={'/login'}>Login</Menu>
            }
          </Menubar>
        </Inner>
      </Wrapper>
    );
  }
}

const Wrapper = styled('div')`
  position: relative;
  background: var(--primary);
  color: white;
  /* box-shadow: 0px 1px 6px 0 rgba(0,0,0,.3), 0px 2px 4px 0 rgba(0,0,0,.1); */
  box-shadow: 0px 1px 2px 0 rgba(0,0,0,.1), 0px 2px 4px 0 rgba(0,0,0,.1);
`

const Inner = styled('div')`
  padding-left: 0;
  padding-right: 0;
`

const Title = styled('h1')`
  margin: 0;
  font-size: 1.3em;
  padding: 10px 15px 3px;
`

const Menubar = styled('div')`
  padding-left: 7px; 
  display: flex;
  overflow-x: auto;

  &::-webkit-scrollbar {
    display: none;
  }
`

const Menu = styled(NavLink)`
  display: block;
  padding: 10px 10px 7px;
  cursor: pointer;
  color: white;
  text-decoration: none;
  border-bottom: 3px solid transparent;

  &.active {
    font-weight: 700;
    border-color: var(--secondary);
  }
`

export default observer(MainNavbar);
