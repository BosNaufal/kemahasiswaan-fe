import { observable, action } from 'mobx';

class ViewModel {
  @observable public show: boolean = true
  @observable public backButton: boolean = true
  @observable public type: string = "main"

  @observable public text: string = "Menu Text"

  @action public setBackButton = (withBackButton: boolean): void => {
    this.backButton = withBackButton
  }

  @action public setType = (type: string): void => {
    this.type = type
  }

  @action public setShow = (isShow: boolean): void => {
    this.show = isShow
  }

  @action public setText = (text: string): void => {
    this.text = text
  }
}

const store = new ViewModel()
export default store
