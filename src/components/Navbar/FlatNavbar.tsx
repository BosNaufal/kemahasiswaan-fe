import * as React from 'react';
import styled from 'react-emotion'
import { observer } from 'mobx-react';

import ViewModel from './ViewModel'

class FlatNavbar extends React.Component<{}, {}> {
  public handleBack = () => {
    window.history.back()
  }

  public render() { 
    return ( 
      <Wrapper>
        <Inner className="mini-container"> 
          {ViewModel.backButton?
            <BackButton onClick={this.handleBack}>
              <i className="ion ion-md-arrow-round-back" />
            </BackButton>
          :null}
          <Title titleOnly={!ViewModel.backButton}>{ViewModel.text}</Title>
        </Inner>
      </Wrapper>
    );
  }
}

const Wrapper = styled('div')`
  position: relative;
  background: var(--primary);
  color: white;
  box-shadow: 0px 1px 2px 0 rgba(0,0,0,.1), 0px 2px 4px 0 rgba(0,0,0,.1);
`

const Inner = styled('div')`
  display: flex;
  padding-left: 0;
  padding-right: 0;
`

const Title = styled('h1')`
  margin: 0;
  font-size: 1.3em;
  padding: 12px 15px 10px;
  padding-left: 0;
  flex: 1;

  ${({ titleOnly }: any) => titleOnly && ({
    paddingLeft: 25,
  })}
`

const BackButton = styled('a')`
  display: flex;
  font-size: 1.7em;
  padding: 0 15px 0 18px;
  align-items: center;
  cursor: pointer;
  line-height: 0;
`

export default observer(FlatNavbar);
