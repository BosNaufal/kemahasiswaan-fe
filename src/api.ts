
declare global {
    interface Window { __MY_API_URL__: any; }
}

import axios from 'axios'
import Cookies from 'js-cookie'

// const baseURL = 'http://beasiswa.bosnaufal.com/be/api'
// const baseURL = 'http://localhost/kemahasiswaan/api'
const baseURL = window.__MY_API_URL__
const fileURL = `${baseURL}/file`

const Req = axios.create({
    baseURL,
    headers: {
        token: Cookies.get("token"),
    }
});

Req.interceptors.response.use((response: any) => {
    return response;
  }, (e: any) => {
    if (!e.response) {
        e.response = {}
    }
    e.response.error = true
    return e.response
  });
  

const updateToken = (newToken: string) => {
    Req.defaults.headers.token = newToken
}

export {
    baseURL,
    fileURL,
    updateToken
}

export default Req;