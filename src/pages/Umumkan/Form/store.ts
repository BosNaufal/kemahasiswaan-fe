import { observable, action } from 'mobx'
import Req from 'src/api'
import { Beasiswa } from 'src/pages/Beasiswa/Detail/store';
import { PendaftarStatus } from 'src/pages/Pendaftar/List/store';

class Store {
  @observable public beasiswa: Beasiswa
  @observable public pendaftarList: any[]
  @observable public loading: boolean = false
  @observable public params: any = {
    page: 1,
    per_page: 1000,
    status: PendaftarStatus.APPROVED,
  }

  @action public setParams = (key: string, payload: any) => {
    this[key] = { ...this.params, ...payload }
  }

  @action public setModel = <K extends keyof Store>(key: K, payload: Store[K]): void => {
    this[key] = payload
  }

  @action setLoading = (payload: boolean) => {
    this.loading = payload
  }

  loadPendaftarList = async (beasiswaId: string) => {
    this.setLoading(true)
    const res = await Req.get('/pendaftar', {
      params: {
        beasiswa_id: beasiswaId,
        ...this.params,
      }
    })
    const { data } = res
    const { results } = data
    this.setModel('pendaftarList', results)

    await this.loadDetailBeasiswa(beasiswaId)

    this.setLoading(false)
  }

  public loadDetailBeasiswa = async (beasiswaId: string): Promise<any> => {
    const res = await Req.get(`/beasiswa/${beasiswaId}`)
    const data: Beasiswa = res.data
    this.setModel('beasiswa', data)
  }

  public unpublishBeasiswa = async (): Promise<any> => {
    const res = await Req.put(`/beasiswa/${this.beasiswa.id}`, {
      ...this.beasiswa,
      published_date: null,
    })
    return res
  }

  public bulkSelesaiPendaftar = async (): Promise<any> => {
    const res = await Req.post(`/pendaftar/selesai/${this.beasiswa.id}`)
    return res
  }

  public umumkan = async (pesan: string): Promise<any> => {
    this.setLoading(true)
    const res = await Req.post(`/umumkan`, {
      pesan,
      penerima: this.pendaftarList.map((pendaftar) => pendaftar.email),
    })
    if (!res.data.error) {
      await this.unpublishBeasiswa()
      await this.bulkSelesaiPendaftar()
    }
    this.setLoading(false)
    return res
  }

}

const store = new Store()
export { Store }
export default store
