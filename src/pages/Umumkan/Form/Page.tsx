import * as React from 'react';
import { hot } from 'react-hot-loader'
import { observer } from 'mobx-react'
import { RouteComponentProps } from 'react-router';

import RichEditor from 'src/components/RichEditor/RichEditor';
import Navigation from 'src/components/Styled/Nav';
import { Button } from 'src/components/Styled/General';
import Breadcrumb from 'src/components/Breadcrumb';

import store from './store';

class UmumkanFormPage extends React.Component<RouteComponentProps, {}> {

  state = {
    ready: false,
    loading: false,
    pesan: "",
  }

  async componentDidMount() {
    const { beasiswaId } = this.props.match.params as any
    await store.loadPendaftarList(beasiswaId)
    await store.loadDetailBeasiswa(beasiswaId)
    this.setState({ 
      ready: true,
      pesan: this.createEmailContent(),
    })
  }

  handleChange = (value: string) => {
    this.setState({ pesan: value })
  }

  handleSubmit = async () => {
    this.setState({ loading: true })
    const res = await store.umumkan(this.state.pesan)
    this.setState({ loading: false })
    setTimeout(() => {
      if (!res.data.error) {
        alert("Email telah dikirim")
        const nextLink = `/beasiswa/detail/${store.beasiswa.id}`
        this.props.history.replace(nextLink)
      } else {
        alert(res.data.messages)
      }
    })
  }

  createEmailContent = () => {
    return `
      <div>Kepada:</div>
      <div><strong>Yth. Penerima Beasiswa</strong></div>
      <div></div>
      <div>Assalamu'alaikum Wr. Wb.</div>
      <div>Selamat, kepada semua penerima beasiswa <strong>${store.beasiswa.judul}</strong> yang namanya tercantum dibawah ini:</div>
      ${this.createHTMLPendaftarList()}
      <div>Silahkan menyerahkan dokumen kepada pihak akademik untuk validasi keaslian berkas</div>
      <div></div>
      <div>Terima kasih atas perhatiannya</div>
      <div>Wassalamu'alaikum Wr. Wb.</div>
    `
  }

  createHTMLPendaftarList = () => {
    if (store.pendaftarList === undefined) {
      return ""
    }
    const listHTML = store.pendaftarList.map((pendaftar) => `<li><strong>${pendaftar.nama}</strong> - ${pendaftar.email}</li>`)
    return `<ul>\n${listHTML.join('\n')}\n</ul>
    `
  }

  public render() { 
    return this.state.ready && (
      <div>
        <div className="mini-container">
          {store.beasiswa.id &&
            <Breadcrumb 
              links={[
                { text: "Home", link: "#/" },
                { text: store.beasiswa.judul, link: `#/beasiswa/detail/${store.beasiswa.id}` },
                { text: "Pendaftar List", link: `#/pendaftar/beasiswa/${store.beasiswa.id}` },
                { text: "Umumkan", link: `#/umumkan/beasiswa/${store.beasiswa.id}`, active: true },
              ]} 
            />
          }
          <Navigation>
            <Button
              onClick={this.handleSubmit} 
              disabled={this.state.loading}
            >
              <i className="ion ion-md-megaphone" />
              <span>{this.state.loading ? 'BROADCASTING...' : 'ANNOUNCE'}</span>
            </Button>
          </Navigation>
          <h2>Pesan Pengumuman</h2>
          <RichEditor 
            defaultValue={this.state.pesan} 
            onChange={this.handleChange} 
          />
        </div>
      </div>
    );
  }
}

export default hot(module)(observer(UmumkanFormPage));