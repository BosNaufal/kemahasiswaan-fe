import * as React from 'react';
import { hot } from 'react-hot-loader'
import { observer } from 'mobx-react'
import { RouteComponentProps } from 'react-router-dom'


import { Input } from 'src/components/Styled/Form'
import { Button } from 'src/components/Styled/General';

import store, { Store } from './store';

class LoginPage extends React.Component<RouteComponentProps, {}> {

  handleInput = (name: keyof Store, e: React.ChangeEvent<HTMLInputElement>) => {
    store.setModel(name, e.target.value)
  }

  handleSubmit = async (e: React.FormEvent) => {
    e.preventDefault()
    const res = await store.login()
    if (res) {
      this.props.history.push('/')
      window.location.reload()
    }
  }

  public render() { 
    return (
      <div className="mini-container">
        <form onSubmit={this.handleSubmit}>
          <Input 
            type="text"
            placeholder="Username" 
            value={store.email} 
            onChange={this.handleInput.bind(null, 'email')}
          />
          <Input 
            type="password"
            placeholder="Password" 
            value={store.password} 
            onChange={this.handleInput.bind(null, 'password')}
          />
          <Button disabled={store.loading}>{store.loading ? "Loading...." : "Login"}</Button>
        </form>
      </div>
    );
  }
}

export default hot(module)(observer(LoginPage));