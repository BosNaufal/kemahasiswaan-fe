import { observable, action } from 'mobx'
import Req, { updateToken } from 'src/api'
import Cookies from 'js-cookie'
import globalStore from '../App/store';

class Store {
  @observable public email: string = ""
  @observable public password: string = ""

  @observable public loading: boolean = false


  @action public setModel = <K extends keyof Store>(key: K, payload: Store[K]): void => {
    this[key] = payload
  }

  @action setLoading = (payload: boolean) => {
    this.loading = payload
  }

  public login = async (): Promise<boolean> => {
    this.setLoading(true)
    const res = await Req.post('/login', {
      email: this.email,
      password: this.password
    })
    if (!res.error) {
      const { data } = res
      globalStore.setUserInfo(data)
      Cookies.set("token", data.token)
      updateToken(data.token)
    } else {
      alert("User Not Found")
    }
    this.setLoading(false)
    return res.error !== true ? true : false
  }
}

const store = new Store()
export { Store }
export default store
