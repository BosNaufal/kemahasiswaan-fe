import { observable, action } from 'mobx'

import Req from 'src/api'

class Store {
  @observable public loading: boolean = false

  @action public setLoading = (payload: boolean) => {
    this.loading = payload
  }

  public loadDetail = async (beasiswaId: string): Promise<any> => {
    await Req.get(`/beasiswa/${beasiswaId}`)
  }
}

const store = new Store()
export default store
