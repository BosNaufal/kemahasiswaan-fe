import * as React from 'react';
import styled from 'react-emotion'
import { 
  BarChart, 
  Bar,
  XAxis, 
  YAxis, 
  CartesianGrid, 
  Tooltip, 
  // LabelList, 
  Legend, 
  ResponsiveContainer
} from 'recharts';

const CustomTooltip  = ({
  active,
  payload,
  label,
}: any) => {
  if (active && payload) {
    return (
      <CustomTooltipElement>
        <p className="intro">{payload[0].payload.title}</p>
        <p className="label" style={{ color: payload[0].fill }}>{`${payload[0].name} : ${payload[0].value}`}</p>
        <p className="label" style={{ color: payload[1].fill }}>{`${payload[1].name} : ${payload[1].value}`}</p>
        <p className="label" style={{ color: payload[2].fill }}>{`${payload[2].name} : ${payload[2].value}`}</p>
      </CustomTooltipElement>
    );
  }

  return null;
}

const CustomTooltipElement = styled('div')`
  background: rgba(255,255,255,1);
  padding: 5px 10px 3px;
  border: 1px solid #DDD;

  p {
    margin-top: 0;
    margin-bottom: 5px;
  }

  .intro {
    font-size: 1.1em;
    font-weight: bold;
  }
` 

const SimpleBarChart = ({
  data = [],
}: any) => {
  return (
    <ResponsiveContainer width={'100%'} minWidth={345} height={'80%'} minHeight={360}>
      <BarChart 
        data={data}
        margin={{top: 5, bottom: 5}}
      >
        <CartesianGrid strokeDasharray="3 3"/>
        <XAxis dataKey="name"/>
        <YAxis/>
        <Tooltip content={<CustomTooltip />} />
        <Legend />
        <Bar dataKey="diterima" fill="#009688" minPointSize={5} />
        <Bar dataKey="ditolak" fill="#e53935" minPointSize={10}/>
        <Bar dataKey="unconfirmed" fill="#8884d8" minPointSize={10}/>
      </BarChart>
    </ResponsiveContainer>
  )
}


export interface GraphProps {
  data: any
  title: string,
}
 
const Graph: React.SFC<GraphProps> = ({
  data,
  title = "Grafik",
}: GraphProps) => {
  return ( 
    <div>
      <GraphTitle>{title}</GraphTitle>
      <SimpleBarChart 
        data={data} 
      />
    </div>
  );
}

const GraphTitle = styled('h2')`
  /* padding-left: 35px; */
`
 
export default Graph;