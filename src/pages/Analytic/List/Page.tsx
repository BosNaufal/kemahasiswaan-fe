import * as React from 'react';
import { hot } from 'react-hot-loader'
import { RouteComponentProps } from 'react-router-dom'
import GenderGraph from './GenderGraph'
import StatusGraph from './StatusGraph'
import JumlahPendaftarGraph from './JumlahPendaftarGraph'
import ScoreGraph from './ScoreGraph'
import StatusGender from './StatusGender'
import Autocomplete from 'src/components/Autocomplete/Autocomplete'
import Req, { baseURL } from 'src/api';
import { Button } from 'src/components/Styled/General';

class AnalyticListPage extends React.Component<RouteComponentProps, any> {
  state = {
    data: [
      // {name: 'Beasiswa A', title: "beasiswa supersemar", 'laki-laki': 4000, 'perempuan': 2400, amt: 2400},
      // {name: 'Beasiswa B', title: 'Beasiswa B', 'laki-laki': 3000, 'perempuan': 1398, amt: 2210},
      // {name: 'Beasiswa C', title: 'Beasiswa C', 'laki-laki': 2000, 'perempuan': 8, amt: 2290},
      // {name: 'Beasiswa D', title: 'Beasiswa D', 'laki-laki': 2780, 'perempuan': 3908, amt: 2000},
      // {name: 'Beasiswa E', title: 'Beasiswa E', 'laki-laki': 18, 'perempuan': 4800, amt: 2181},
      // {name: 'Beasiswa F', title: 'Beasiswa F', 'laki-laki': 2390, 'perempuan': 3800, amt: 2500},
      // {name: 'Beasiswa G', title: 'Beasiswa G', 'laki-laki': 3490, 'perempuan': 4300, amt: 2100},
    ],
    years: [],
    form: {
      beasiswas: [],
      years: [],
    },
    loading: false,
    scoreData: [],
  }

  componentDidMount() {
    this.refreshScoreGraph()
    const year = (new Date()).getFullYear()
    const years = (new Array(10)).fill("").map((a, index) => {
      return {
        label: year - index,
        value: year - index,  
      }
    })
    this.setState({ years })
  }

  getValuePath = (res: any) => res.data.results

  loadYears = async () => this.state.years

  refreshScoreGraph = async () => {
    const res = await Req.get('/analytic/scoreboard')
    if (res.data.results) {
      const { results } = res.data
      const scoreData = results.map((beasiswa: any) => ({
          name: beasiswa.judul,
          'pendaftar_aktif': beasiswa.pendaftar_aktif,
          'pendaftar_selesai': beasiswa.pendaftar_selesai,
          'semua_pendaftar': beasiswa.all_pendaftar,
      }))
      this.setState({ scoreData })
    }
  }

  handleChangeAutocomplete = (name: string, selectedValue: any) => {
    this.setState(() => {
      const newState = {...this.state}
      newState.form = {
        ...this.state.form,
        [name]: selectedValue.map 
          ? selectedValue.map((option: any) => option.value)
          : [selectedValue.value]
      }
      return newState 
    })
  }

  handleSubmit = async () => {
    this.setState({ loading: true })
    const res = await Req.post('/analytic', {
      ...this.state.form
    })
    this.setState({ loading: false })
    if (res.data.length) {
      const data: any = []
      res.data.map((beasiswa: any) => {
        const baseData = {
          title: beasiswa.judul,
        }

        beasiswa.analytics.map((graph: any) => {
          const perempuan = 'perempuan'
          const statusAndGender = graph.status_and_gender
          const statusAndGenderKeys = Object.keys(statusAndGender)
          const statusAndGenderData = {}
          statusAndGenderKeys.forEach((statusKey) => {
            statusAndGenderData[statusKey] = statusAndGender[statusKey]
          })
          data.push({
            ...baseData,
            name: graph.tahun,
            'laki-laki': graph.gender['laki-laki'],
            'perempuan': graph.gender[perempuan],
            'diterima': graph.status.diterima,
            'ditolak': graph.status.ditolak,
            'unconfirmed': graph.status.unconfirmed,
            'total': graph.jumlah,
            ...statusAndGenderData,
          })
        })

      })
      this.setState({ data })
    }
  }
  
  public render() { 
    return (
      <div className="mini-container">
        {this.state.scoreData.length !== 0 &&
          <ScoreGraph 
            data={this.state.scoreData} 
            title="Peringkat Beasiswa Pendaftar Terbanyak"
          />
        }

        <div style={{ marginBottom: 55 }} />

        <Autocomplete 
          isMulti={false}
          url={`${baseURL}/beasiswa`}
          getValuePath={this.getValuePath}
          paramName="search"
          valueKey="id"
          labelKey="judul"
          onChange={this.handleChangeAutocomplete.bind(null, "beasiswas")}
        />
        {this.state.years.length &&
          <Autocomplete 
            isMulti={true}
            options={this.state.years}
            onLoadOptions={this.loadYears}
            onChange={this.handleChangeAutocomplete.bind(null, "years")}
          />
        }

        <Button onClick={this.handleSubmit} disabled={this.state.loading}>{this.state.loading ? "Loading..." : "Submit"}</Button>

        {this.state.data.length !== 0 &&
          <div>
            <div style={{ marginTop: 35 }} />
            <GenderGraph 
              data={this.state.data} 
              title="Grafik Gender"
            />
            <StatusGraph 
              data={this.state.data} 
              title="Grafik Jumlah Pendaftar"
            />
            <JumlahPendaftarGraph 
              data={this.state.data} 
              title="Grafik Status Pendaftar"
            />
            <StatusGender 
              data={this.state.data} 
              title="Grafik Status Pendaftar"
            />
            <div style={{ marginBottom: 15 }} />
          </div>
        }

      </div>
    );
  }
}

export default hot(module)(AnalyticListPage);