import * as React from 'react';
import styled from 'react-emotion'
import { 
  BarChart, 
  Bar,
  XAxis, 
  YAxis, 
  CartesianGrid, 
  Tooltip, 
  // LabelList, 
  Legend, 
  ResponsiveContainer
} from 'recharts';

const CustomTooltip  = ({
  active,
  payload,
  label,
}: any) => {
  if (active && payload) {
    return (
      <CustomTooltipElement>
        <p className="intro">{payload[0].payload.title}</p>
        <p className="label" style={{ color: payload[0].fill }}>{`${payload[0].name} : ${payload[0].value}`}</p>
        <p className="label" style={{ color: payload[1].fill }}>{`${payload[1].name} : ${payload[1].value}`}</p>
        <p className="label" style={{ color: payload[2].fill }}>{`${payload[2].name} : ${payload[2].value}`}</p>

        <p className="label" style={{ color: payload[3].fill }}>{`${payload[3].name} : ${payload[3].value}`}</p>
        <p className="label" style={{ color: payload[4].fill }}>{`${payload[4].name} : ${payload[4].value}`}</p>
        <p className="label" style={{ color: payload[5].fill }}>{`${payload[5].name} : ${payload[5].value}`}</p>
      </CustomTooltipElement>
    );
  }

  return null;
}

const CustomTooltipElement = styled('div')`
  background: rgba(255,255,255,1);
  padding: 5px 10px 3px;
  border: 1px solid #DDD;

  p {
    margin-top: 0;
    margin-bottom: 5px;
  }

  .intro {
    font-size: 1.1em;
    font-weight: bold;
  }
` 

const SimpleBarChart = ({
  data = [],
}: any) => {
  return (
    <ResponsiveContainer width={'100%'} minWidth={345} height={'80%'} minHeight={360}>
      <BarChart 
        data={data}
        margin={{top: 5, bottom: 5}}
      >
       <CartesianGrid strokeDasharray="3 3"/>
       <XAxis dataKey="name"/>
       <YAxis/>
       <Tooltip content={<CustomTooltip />} />
       <Legend />
       <Bar dataKey="diterima_laki_laki" stackId="a" fill="#2f9687" />
       <Bar dataKey="diterima_perempuan" stackId="a" fill="#87ca98" />

       <Bar dataKey="ditolak_laki_laki" stackId="b" fill="#e03c2f" />
       <Bar dataKey="ditolak_perempuan" stackId="b" fill="#e0812f" />

       <Bar dataKey="unconfirmed_laki_laki" stackId="c" fill="#585858" />
       <Bar dataKey="unconfirmed_perempuan" stackId="c" fill="#adadad" />
      </BarChart>
    </ResponsiveContainer>
  )
}


export interface GraphProps {
  data: any
  title: string,
}
 
const Graph: React.SFC<GraphProps> = ({
  data,
  title = "Grafik",
}: GraphProps) => {
  return ( 
    <div>
      <GraphTitle>{title}</GraphTitle>
      <SimpleBarChart 
        data={data} 
      />
    </div>
  );
}

const GraphTitle = styled('h2')`
  /* padding-left: 35px; */
`
 
export default Graph;