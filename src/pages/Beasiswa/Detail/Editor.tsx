import * as React from 'react';

import RichEditor from 'src/components/RichEditor'

export interface EditorProps {
  defaultValue?: string
  onChange(html: string): void
}

const Editor: React.SFC<EditorProps> = (props) => (
  <RichEditor affixOffset={120} {...props} />
)
 
export default Editor;
