import * as React from 'react';
import { hot } from 'react-hot-loader'
import { observer } from 'mobx-react'
import { RouteComponentProps } from 'react-router';

import PostPageNav from './Nav';

import globalStore from 'src/pages/App/store';
import store from './store';
import BeasiswaForm from './BeasiswaForm';
import CreateForm from 'src/pages/Form/Create/components/CreateForm';
import CreateFormModel from 'src/pages/Form/Create/components/CreateFormModel';
import FormFill from 'src/pages/Form/Fill/components/FormFill';
import BeasiswaDetail from './BeasiswaDetail';
import FormFillModel from 'src/pages/Form/Fill/components/FormFillModel';
import Loading from 'src/components/Loading/Loading';

class PostPage extends React.Component<RouteComponentProps, {}> {

  formModel: CreateFormModel

  state = {
    ready: false,
    isSubmitting: false,
  }

  async componentDidMount() {
    store.netralize()
    const { beasiswaId } = this.props.match.params as any
    if (beasiswaId) {
      await store.loadDetail(beasiswaId)
    }
    if (globalStore.isLogged) {
      store.setScreen('postEditor')
    }
    this.setState({ ready: true })
  }

  handleFormCreatorRef = (component: any) => {
    if (!this.formModel && component) {
      this.formModel = component.model
    }
  }

  prepareForms = () => {
    if (this.formModel) {
      const formAsString = JSON.stringify(this.formModel.validForms)
      store.setBeasiswa('form', formAsString)
    }
  }
  
  handleDaftar = async (formFill: FormFillModel) => {
    this.setState({ isSubmitting: true })
    const isFormValid = formFill.hasFilledRequiredFields
    if (!isFormValid) {
      alert("Form Belum Lengkap")
      this.setState({ isSubmitting: false })
      return false
    }
    const jawaban = formFill.validForms;
    const res = await store.daftarBeasiswa({
      nama: jawaban.find((jwb) => jwb.question === "Nama")!.value,
      email: jawaban.find((jwb) => jwb.question === "Email")!.value,
      gender: jawaban.find((jwb) => jwb.question === "Jenis Kelamin")!.checkedValue[0].text,
      jawaban: JSON.stringify(formFill.validForms),
    })

    this.setState({ isSubmitting: false })

    if (res.data.error) {
      alert(res.data.messages[0])
      return false
    }
    store.setScreen('detail')
    return true
  }


  public render() { 

    if (!this.state.ready) {
      return <Loading />
    }

    return this.state.ready && (
      <div>
        <div className="mini-container">
          <PostPageNav onBeforeSave={this.prepareForms} />
          {(() => {
            switch (store.screen) {
              case "formCreator":
                return (
                  <CreateForm 
                    ref={this.handleFormCreatorRef} 
                    defaultValue={store.beasiswa.form}
                  />
                )
              case "postEditor":
                return <BeasiswaForm />
              case "formFill":
                return (
                  <FormFill 
                    form={store.validBeasiswaForm} 
                    onSubmit={this.handleDaftar} 
                    isSubmitting={this.state.isSubmitting}
                  />
                )
              default: 
                return (
                  <BeasiswaDetail 
                    beasiswa={store.beasiswa} 
                  />
                )
            }
          })()}
        </div>
      </div>
    );
  }
}

export default hot(module)(observer(PostPage));