import * as React from 'react';
import styled from 'react-emotion'
import { observer } from 'mobx-react'

class DetailBeasiswa extends React.Component<any, {}> {
  public render() { 
    const { beasiswa } = this.props
    return (
      <div>
        <Title>{beasiswa.judul}</Title>
        <Content dangerouslySetInnerHTML={{ __html: beasiswa.konten }} />
      </div>
    );
  }
}

const Title = styled('h1')`
  margin-top: 15px;
  margin-bottom: 20px;
  font-size: 1.6em;
  font-weight: 700;
`

const Content = styled('div')`
  font-size: 1.1em;
  line-height: 1.5;
`

export default observer(DetailBeasiswa);