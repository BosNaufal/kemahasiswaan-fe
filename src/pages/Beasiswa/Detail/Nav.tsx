import * as React from 'react';
import { observer } from 'mobx-react';
import { withRouter, RouteComponentProps } from 'react-router-dom'

import { Button } from 'src/components/Styled/General'

import globalStore from 'src/pages/App/store';
import store from './store';
import Navigation from 'src/components/Styled/Nav';

export interface PostPageNavProps {
  onBeforeSave(): void
} 

class PostPageNav extends React.Component<PostPageNavProps & RouteComponentProps, {}> {
  
  redirect = (routePath?: string) => {
    this.props.history.push(routePath || `/`)
  }

  handleSubmit = async () => {
    this.props.onBeforeSave()
    const res = await store.submitBeasiswa()
    if (res.data.id) {
      this.redirect(`/beasiswa/detail/${res.data.id}`)
    } else {
      alert("Something wrong!")
      this.redirect()
    }
  }

  handlePublish = async () => {
    this.props.onBeforeSave()
    const res = await store.publishBeasiswa()
    console.log(res) 
    // this.redirect()
  }

  handleUnpublish = async () => {
    const sureToDelete = confirm("Yakin ingin menghapus?")
    if (sureToDelete) {
      await store.unpublishBeasiswa()
      this.redirect()
    }
  }

  changeScreen = (screen: string) => {
    store.setScreen(screen)
  }

  handlePendaftaranList = () => {
    this.redirect(`/pendaftar/beasiswa/${store.beasiswa.id}`)
  }

  isActive = (name: string) => (
    store.screen === name
  )

  render() { 
    const { isActive } = this
    
    return !store.loading ? (
      <div>
        {globalStore.isLogged ?
          <Navigation>
            <Button onClick={this.handleSubmit} disabled={!store.formIsValid}>
              <i className="ion ion-md-save" />
              <span>SAVE</span>
            </Button>
            <Button onClick={this.changeScreen.bind(null, 'postEditor')} inverse={isActive("postEditor")}>
              <i className="ion ion-md-paper" />
              <span>POST</span>
            </Button>
            <Button onClick={this.changeScreen.bind(null, 'formCreator')} inverse={isActive("formCreator")}>
              <i className="ion ion-md-create" />
              <span>FORM</span>
            </Button>
            {store.isEdit &&
              <Button onClick={this.handlePendaftaranList}>
                <i className="ion ion-md-people" />
                <span>PENDAFTAR</span>
              </Button>
            }
            <Button onClick={this.changeScreen.bind(null, 'detail')} inverse={isActive("detail")} disabled={!store.formIsValid}>
              <i className="ion ion-md-search" />
              <span>PREVIEW</span>
            </Button>

            {store.beasiswa.published_date !== null ?
              <Button danger={true} onClick={this.handleUnpublish}>
                <i className="ion ion-md-trash" />
                <span>UNPUBLISH</span>
              </Button>
            :
              <Button onClick={this.handlePublish} disabled={!store.formIsValid}>
                <i className="ion ion-md-megaphone" />
                <span>PUBLISH</span>
              </Button>
            }
          </Navigation>
        : 
          <Navigation>
            <Button onClick={this.changeScreen.bind(null, 'detail')} inverse={isActive("detail")}>
              <i className="ion ion-md-information-circle-outline" />
              <span>INFO</span>
            </Button>
            {store.beasiswa.form &&
              <Button onClick={this.changeScreen.bind(null, 'formFill')} inverse={isActive("formFill")}>
                <i className="ion ion-md-paper" />
                <span>DAFTAR</span>
              </Button>
            }
          </Navigation>
        }
      </div>     
    ) : 
      <div>
        <div>Loading....</div>
      </div>
    ;
  }
}
 
export default withRouter(observer(PostPageNav));