import * as React from 'react';
import styled from 'react-emotion'
import { observer } from 'mobx-react'

import { Input } from 'src/components/Styled/Form'
import Editor from './Editor'

import store from './store';

class BeasiswaForm extends React.Component<{}, {}> {

  handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    store.setBeasiswa('judul', e.target.value)
  }

  public render() { 
    return (
      <div>
        <StyledInput 
          placeholder="Judul" 
          value={store.beasiswa.judul} 
          onChange={this.handleInput}
        />
        <Editor defaultValue={store.beasiswa.konten} onChange={store.setBeasiswa.bind(null, 'konten')} />
      </div>
    );
  }
}


const StyledInput = styled(Input)`
  margin-top: 15px;
  margin-bottom: 20px;
  border: 0;
  font-size: 1.6em;
  font-weight: 700;
  padding: 0 5px;
`


export default observer(BeasiswaForm);