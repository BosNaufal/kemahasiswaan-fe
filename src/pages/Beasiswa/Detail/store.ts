import { observable, action, computed } from 'mobx'
import * as moment from 'moment'

import Req from 'src/api'

export interface Beasiswa {
  id?: string
  judul: string
	konten: string
	published_date: string | null
	form?: string
  email_body?: string,
  has_approved_pendaftar?: number
}

export interface Pendaftar {
  beasiswa_id?: string
  nama: string
  email: string
  gender?: string
  status?: string
  jawaban: string
}

class Store {
  @observable public beasiswa: Beasiswa = {
    judul: "",
    konten: "",
    published_date: null
  };

  @observable public loading: boolean = false
  @observable public screen: string = "detail"

  @computed get isEdit(): boolean {
    return this.beasiswa.id !== undefined
  }

  @computed get formIsValid(): boolean {
    return this.beasiswa.konten !== "" && this.beasiswa.judul !== ""
  } 

  @computed get validBeasiswaForm(): string {
    const beasiswaForm = JSON.parse(this.beasiswa.form!)    

    const requiredFields = [
      {
        id: 'required-nama',
        question: "Nama",
        required: true,
        type: "text",
      },
      {
        id: 'required-email',
        question: "Email",
        required: true,
        type: "text",
      },
      {
        id: 'required-gender',
        question: "Jenis Kelamin",
        required: true,
        type: "radio",
        answers: [
          { 
            text: "laki-laki",
            checked: false,
          },
          { 
            text: "perempuan",
            checked: false,
          },
        ]
      },
    ]

    const newBeasiswaForm = requiredFields.concat(beasiswaForm)
    return JSON.stringify(newBeasiswaForm)
  }

  @action netralize = () => {
    this.beasiswa = {
      judul: "",
      konten: "",
      published_date: null
    };
    this.screen = "detail"
  }

  @action public setScreen = (payload: string) => {
    this.screen = payload
  }

  @action public setLoading = (payload: boolean) => {
    this.loading = payload
  }

  @action public setBeasiswa = <K extends keyof Beasiswa>(key: K, payload: Beasiswa[K]): void => {
    this.beasiswa[key] = payload
  }

  @action public replaceBeasiswa = (payload: Beasiswa) => {
    this.beasiswa = payload
  }

  public loadDetail = async (beasiswaId: string): Promise<any> => {
    const res = await Req.get(`/beasiswa/${beasiswaId}`)
    const data: Beasiswa = res.data
    this.replaceBeasiswa(data)
  }

  public submitBeasiswa = (): Promise<any> => {
    if (this.isEdit) {
      return this.updateBeasiswa()
    } else {
      return this.createBeasiswa()
    }
  }

  public createBeasiswa = async (): Promise<any> => {
    this.setLoading(true)
    const res = await Req.post('/beasiswa', {
      ...this.beasiswa
    })
    this.setLoading(false)
    return res
  }

  public updateBeasiswa = async (): Promise<any> => {
    this.setLoading(true)
    const res = await Req.put(`/beasiswa/${this.beasiswa.id}`, {
      ...this.beasiswa
    })
    this.setLoading(false)
    return res
  }

  @action public publishBeasiswa = () => {
    this.beasiswa.published_date = moment().format('YYYY-MM-DD')
    this.submitBeasiswa()
  }

  public unpublishBeasiswa = async (): Promise<any> => {
    this.setLoading(true)
    const res = await Req.put(`/beasiswa/${this.beasiswa.id}`, {
      ...this.beasiswa,
      published_date: null,
    })
    this.setLoading(false)
    return res
  }

  public daftarBeasiswa = async (pendaftar: Pendaftar): Promise<any> => {
    this.setLoading(true)
    const res = await Req.post('/pendaftar', {
      ...pendaftar,
      beasiswa_id: this.beasiswa.id,
    })
    this.setLoading(false)
    return res
  }
}

const store = new Store()
export default store
