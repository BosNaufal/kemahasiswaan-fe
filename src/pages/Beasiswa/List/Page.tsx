import * as React from 'react';
import { hot } from 'react-hot-loader'
import { RouteComponentProps } from 'react-router-dom'

import BeasiswaList from './BeasiswaList';

import globalStore from 'src/pages/App/store'
import store from './store';
import Navigation from 'src/components/Styled/Nav';
import { Button } from 'src/components/Styled/General';
import Searchbar from 'src/components/Searchbar/Searchbar';

interface PostListPageStatus {
  status: string
}

class PostListPage extends React.Component<RouteComponentProps, PostListPageStatus> {

  constructor(props: any) {
    super(props);
    const isLogged = globalStore.userInfo && globalStore.userInfo.id
    this.state = {
      status: !isLogged ? "published" : "all",
    }    
  }

  componentDidMount() {
    store.netralizeParams()
    this.loadBeasiswaList(1)
  }

  loadBeasiswaList = (page = 1) => {
    store.setParams({ page, status: this.state.status })
    store.loadBeasiswaList()
  }

  prepareSearch = () => {
    //
  }

  filterByStatus = (status: string) => {
    let nextStatus: any = status
    if (this.state.status === status) {
      nextStatus = 'all'
    }
    this.setState({ status: nextStatus }, () => {
      this.loadBeasiswaList(1)
    })
  }

  isActive = (status: string) => {
    return this.state.status === status
  }

  handleSearch = (query: string) => {
    store.setParams({ search: query })
    store.loadBeasiswaList()
  }

  public render() { 
    const isLogged = globalStore.userInfo && globalStore.userInfo.id

    return (
      <div className="mini-container">
        <Navigation>
          {isLogged &&
            <React.Fragment>
              <Button 
                active={this.isActive("published")}
                onClick={this.filterByStatus.bind(null, "published")}
              >
                PUBLISHED
              </Button>
              <Button 
                active={this.isActive("unpublished")}
                onClick={this.filterByStatus.bind(null, "unpublished")} 
                danger={true}
              >
                UNPUBLISHED
              </Button>
            </React.Fragment>
          }
        </Navigation>
        <Searchbar onSubmit={this.handleSearch} />
        <div style={{ marginBottom: 15 }} />
        <BeasiswaList />
      </div>
    );
  }
}

export default hot(module)(PostListPage);