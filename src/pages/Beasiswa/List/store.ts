import { observable, action } from 'mobx'
import Req from 'src/api'

class Store {
  @observable public beasiswaList: [] = []
  @observable public beasiswaListMeta: any
  @observable public loading: boolean = false
  @observable public params: any = {
    page: 1,
    per_page: 10,
  }

  @action public setParams = (payload: any) => {
    this.params = { ...this.params, ...payload }
  }

  @action public netralizeParams = () => {
    this.params = {
      page: 1,
      per_page: 10,
    }
  }
  
  @action setLoading = (payload: boolean) => {
    this.loading = payload
  }

  @action public setBeasiswaList = (newList: []) => {
    this.beasiswaList = newList
  }

  @action public setBeasiswaListMeta = (meta: any) => {
    this.beasiswaListMeta = meta
  }

  public loadBeasiswaList = async (): Promise<boolean> => {
    this.setLoading(true)
    const res = await Req.get('/beasiswa', {
      params: this.params,
    })
    const { data } = res
    const { results, meta } = data
    this.setBeasiswaList(results)
    this.setBeasiswaListMeta(meta)

    this.setLoading(false)
    return res
  }
}

const store = new Store()
export { Store }
export default store
