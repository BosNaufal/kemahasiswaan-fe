import * as React from 'react';
import styled from 'react-emotion'
import * as moment from 'moment'
import { Link } from 'react-router-dom'
import { observer } from 'mobx-react';
import strip from 'strip'

import Pagination from 'src/components/Pagination'
import Loading from 'src/components/Loading/Loading';

import store from './store';

class BeasiswaList extends React.Component<{}, {}> {
  handleChangePage = (pageNumber: number) => {
    store.setParams({ page: pageNumber })
    store.loadBeasiswaList()
  }

  public render() { 
    return (
      <div>
        {store.loading ?
          <Loading />
        :
          <div>
            {store.beasiswaList.map((item: any, index: number) => (
              <Item key={index}>
                <Title><Link to={`/beasiswa/detail/${item.id}`}>{item.judul}</Link></Title>
                <DateText>
                  {item.published_date ?  
                    moment(item.published_date).format('D MMMM YYYY')
                    : "Belum dipublish"
                  }
                </DateText>
                <Description>{strip(item.konten).substr(0, 250)}...</Description>
              </Item>
            ))}
            {store.beasiswaListMeta &&
              <Pagination
                active={store.beasiswaListMeta.page}
                perPage={store.beasiswaListMeta.per_page}
                totalData={store.beasiswaListMeta.total_data}
                onChange={this.handleChangePage}
              />
            }
          </div>
        }
      </div>
    );
  }
}

const Item = styled('div')`
  border: 1px solid #DDD;
  border-radius: 5px;
  padding: 10px 15px;
  margin-bottom: 15px;
`

const Title = styled('h2')`
  margin: 5px 0;

  a {
    color: black;
    text-decoration: none;
  }
`

const DateText = styled('div')`
  color: #888;
`

const Description = styled('div')`

`

export default observer(BeasiswaList);