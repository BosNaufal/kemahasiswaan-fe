import { observable, action, computed } from 'mobx'

import Req from 'src/api'

interface Pendaftar {
  id?: string
  beasiswa: any
  beasiswa_id?: string
  nama: string
  email: string
  status?: string | null
  jawabans: any
  jawaban?: any
}

class Store {
  @observable public beasiswa: any
  @observable public pendaftar: Pendaftar
  @observable public loading: boolean = false

  @computed get jawabansById() {
    const group = {
    }
    this.pendaftar.jawabans.forEach((item: any) => {
      if (item.question_id) {
        group[item.question_id] = item
      }
    });
    return group
  }
  
  // Make All filled field has same order with the beasiswa forms 
  @computed get combinedForms() {
    if (this.pendaftar && this.pendaftar.beasiswa && this.pendaftar.beasiswa.form) {
      const beasiswaForm = JSON.parse(this.pendaftar.beasiswa.form!)    

      const newBeasiswaForm = beasiswaForm
        .map((field: any) => {
          if (this.jawabansById[field.id]) {
            return this.jawabansById[field.id]
          }
          return null
        })
      return [
        this.jawabansById['required-nama'],
        this.jawabansById['required-email'],
        ...newBeasiswaForm
      ]
    }
    return []
  }

  @action public replacePendaftar = (payload: Pendaftar) => {
    this.pendaftar = payload
    this.beasiswa = payload.beasiswa
  }

  @action public removeStatus = () => {
    this.pendaftar.status = null
  }

  @action netralize = () => {
    this.pendaftar = {
      beasiswa: {},
      nama: "",
      email: "",
      status: null,
      jawabans: "",
    }
  }
  
  @action public loadDetail = async (pendaftarId: string): Promise<any> => {
    const res = await Req.get(`/pendaftar/${pendaftarId}`)
    const data: Pendaftar = res.data
    this.replacePendaftar(data)
  }
}

const store = new Store()
export default store
