import * as React from 'react';
import { hot } from 'react-hot-loader'
import { observer } from 'mobx-react'
import { RouteComponentProps } from 'react-router';

import FormFill from 'src/pages/Form/Fill/components/FormFill';
import store from './store';
import Breadcrumb from 'src/components/Breadcrumb';

class PendaftarDetailPage extends React.Component<RouteComponentProps, {}> {

  state = {
    ready: false,
    loading: false,
  }

  async componentDidMount() {
    store.netralize()
    const { pendaftarId } = this.props.match.params as any
    if (pendaftarId) {
      await store.loadDetail(pendaftarId)
    }
    this.setState({ ready: true })
  }

  public render() { 
    return this.state.ready && (
      <div>
        <div className="mini-container">
          {store.beasiswa.id && store.pendaftar.id &&
            <Breadcrumb
              links={[
                { text: "Home", link: "#/" },
                { text: store.beasiswa.judul, link: `#/beasiswa/detail/${store.beasiswa.id}` },
                { text: "Pendaftar List", link: `#/pendaftar/beasiswa/${store.beasiswa.id}` },
                { text: store.pendaftar.nama, link: `#/pendaftar/${store.pendaftar.id}`, active: true },
              ]} 
            />
          }
          <h2>Form Detail</h2>
          <FormFill
            readOnly={true}
            form={store.combinedForms} 
          />
        </div>
      </div>
    );
  }
}

export default hot(module)(observer(PendaftarDetailPage));