import { observable, action } from 'mobx'
import Req from 'src/api'
import { Beasiswa } from 'src/pages/Beasiswa/Detail/store';

export enum PendaftarStatus {
  APPROVED = 'diterima',
  DECLINED = 'ditolak',
}

class Store {
  @observable public beasiswa: Beasiswa
  @observable public pendaftarList: any[]
  @observable public pendaftarListMeta: any
  @observable public loading: boolean = false
  @observable public params: any = {
    page: 1,
    per_page: 10,
  }

  @action public setParams = (payload: any) => {
    this.params = { ...this.params, ...payload }
  }

  @action public netralizeParams = () => {
    this.params = {
      page: 1,
      per_page: 10,
    }
  }

  @action public setModel = <K extends keyof Store>(key: K, payload: Store[K]): void => {
    this[key] = payload
  }

  @action setLoading = (payload: boolean) => {
    this.loading = payload
  }

  findIndexItem = (pendaftar: any): number => {
    const targetIndex = this.pendaftarList.findIndex((item) => item.id === pendaftar.id)
    return targetIndex
  }

  @action setLoadingAtItem = (pendaftar: any, payload: boolean) => {
    const targetIndex = this.findIndexItem(pendaftar)
    this.updateItemAtIndex(targetIndex, {
      loading: payload,
    })
  }

  @action updateItemAtIndex = (index: number, payload: any) => {
    const newPendaftarList = [...this.pendaftarList]
    newPendaftarList[index] = {
      ...this.pendaftarList[index],
      ...payload,
    }
    this.pendaftarList = newPendaftarList
  }

  @action updateItem = (pendaftar: any, payload: any) => {
    const index = this.findIndexItem(pendaftar)
    this.updateItemAtIndex(index, payload)
  }

  loadPendaftarList = async (beasiswaId: string) => {
    this.setLoading(true)
    const res = await Req.get('/pendaftar', {
      params: {
        beasiswa_id: beasiswaId,
        ...this.params,
      }
    })
    const { data } = res
    const { results, meta } = data
    this.setModel('pendaftarList', results)
    this.setModel('pendaftarListMeta', meta)

    await this.loadDetailBeasiswa(beasiswaId)

    this.setLoading(false)
  }

  public loadDetailBeasiswa = async (beasiswaId: string): Promise<any> => {
    const res = await Req.get(`/beasiswa/${beasiswaId}`)
    const data: Beasiswa = res.data
    this.setModel('beasiswa', data)
  }

  reqChangePendaftarStatus = async(status: PendaftarStatus, pendaftar: any) => {
    const res = await Req.put(`/pendaftar/${pendaftar.id}`, {
      ...pendaftar,
      status,
      last_status: status,
    })
    const { data } = res
    return data
  }

  changePendaftarStatus = async (status: PendaftarStatus, pendaftar: any) => {
    this.setLoadingAtItem(pendaftar, true)
    const data = await this.reqChangePendaftarStatus(status, pendaftar)
    if (!data.error) {
      this.updateItem(pendaftar, {
        status: data.status,
      })
    }
    this.setLoadingAtItem(pendaftar, false)
  }

}

const store = new Store()
export { Store }
export default store
