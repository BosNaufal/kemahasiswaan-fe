import * as React from 'react';
import styled from 'react-emotion'
import { Link } from 'react-router-dom';

import { PendaftarStatus } from './store';
import ActionButton from './ActionButton';

export interface PendaftarItemProps {
  pendaftar: any
  onApprove(pendaftar: any): void
  onDecline(pendaftar: any): void
  onChange(pendaftar: any): void
}
 
const PendaftarItem: React.SFC<PendaftarItemProps> = ({
  pendaftar,
  onApprove,
  onDecline,
  onChange,
}) => {
  return ( 
    <Item>
      <Title>
        <Link to={`/pendaftar/${pendaftar.id}`}>{pendaftar.nama}</Link>
      </Title>
      <EmailText>{pendaftar.email}</EmailText>
      <CreatedDate>{pendaftar.created_by}</CreatedDate>
      {pendaftar.status &&
        <StateText status={pendaftar.status}>{pendaftar.status}</StateText>
      }
      <ActionButton 
        loading={pendaftar.loading}
        pendaftar={pendaftar}
        onChange={onChange}
        onApprove={onApprove}
        onDecline={onDecline}
      />
    </Item>
  );
}

const Item = styled('div')`
  border: 1px solid #DDD;
  border-radius: 5px;
  padding: 10px 15px;
  margin-bottom: 15px;
`

const Title = styled('h2')`
  margin: 5px 0;
  margin-bottom: 0;

  a {
    color: black;
    text-decoration: none;
  }
`

const EmailText = styled('div')`
  color: #888;
`

const CreatedDate = styled('div')`

`
const StateText = styled('div')`
  font-weight: 700;
  font-size: 1.2em;
  text-transform: uppercase;
  color: white;
  display: inline-block;
  padding: 1px 3px;
  margin-top: 5px;
  ${({ status }: { status: PendaftarStatus }) => ({
    background: status === PendaftarStatus.APPROVED ? 'var(--primary)' : 'var(--red)',
  })}
`

export default PendaftarItem;