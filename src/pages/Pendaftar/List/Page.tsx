import * as React from 'react';
import { hot } from 'react-hot-loader'
import { observer } from 'mobx-react'
import { RouteComponentProps } from 'react-router-dom'

import Loading from 'src/components/Loading/Loading';
import Breadcrumb from 'src/components/Breadcrumb';
import { Button } from 'src/components/Styled/General';

import store, { PendaftarStatus } from './store';
import PendaftarItem from './PendaftarItem';
import Navigation from 'src/components/Styled/Nav';
import Pagination from 'src/components/Pagination';
import Searchbar from 'src/components/Searchbar/Searchbar';

class PendaftarListPage extends React.Component<RouteComponentProps, {}> {

  state = {
    status: undefined,
  }

  componentDidMount() {
    store.netralizeParams()
    this.loadPendaftarList()
  }

  loadPendaftarList = () => {
    const { beasiswaId } = this.props.match.params as any
    store.setParams({ status: this.state.status })
    store.loadPendaftarList(beasiswaId)
  }

  handleApprove = (pendaftar: any) => {
    store.changePendaftarStatus(PendaftarStatus.APPROVED, pendaftar)
  }
  
  handleDecline = (pendaftar: any) => {
    store.changePendaftarStatus(PendaftarStatus.DECLINED, pendaftar)
  }

  handleChange = (pendaftar: any) => {
    store.updateItem(pendaftar, {
      status: null,
    })
  }

  prepareSendEmail = () => {
    const { beasiswaId } = this.props.match.params as any
    this.props.history.push(`/umumkan/beasiswa/${beasiswaId}`)
  }

  handleChangePage = (pageNumber: number) => {
    store.setParams({ page: pageNumber })
    this.loadPendaftarList()
  }

  filterByStatus = (status: string) => {
    let nextStatus: any = status
    if (this.state.status === status) {
      nextStatus = undefined
    }
    this.setState({ status: nextStatus }, () => {
      this.loadPendaftarList()
    })
  }

  isActive = (status: string) => {
    return this.state.status === status
  }

  handleSearch = (query: string) => {
    store.setParams({ search: query })
    this.loadPendaftarList()
  }

  public render() { 
    return (
      <div className="mini-container">
        {store.beasiswa &&
          <Breadcrumb 
            links={[
              { text: "Home", link: "#/" },
              { text: store.beasiswa.judul, link: `#/beasiswa/detail/${store.beasiswa.id}` },
              { text: "Pendaftar List", link: `#/pendaftar/beasiswa/${store.beasiswa.id}`, active: true },
            ]} 
          />
        }

        {store.beasiswa &&
          <div>
            <h2 style={{ marginBottom: 7, marginLeft: 5 }}>{store.beasiswa.judul}</h2>
            <Navigation>
              {store.beasiswa.has_approved_pendaftar && store.beasiswa.has_approved_pendaftar > 0 &&
                <Button onClick={this.prepareSendEmail}>ANNOUNCE</Button>
              }
              <Button
                onClick={this.filterByStatus.bind(null, "null")}
                active={this.isActive("null")}
              >
                UNCONFIRM
              </Button>
              <Button
                onClick={this.filterByStatus.bind(null, "diterima")}
                active={this.isActive("diterima")}
              >
                APPROVED
              </Button>
              <Button
                onClick={this.filterByStatus.bind(null, "ditolak")}
                active={this.isActive("ditolak")}
                danger={true}
              >
                REJECTED
              </Button>
            </Navigation>
            <Searchbar onSubmit={this.handleSearch} />
            <div style={{ marginBottom: 15 }} />
          </div>
        }

        {store.loading
          ? <Loading />
          : store.pendaftarList && store.pendaftarList.length > 0 ? 
            <div>
              {store.pendaftarList.map((pendaftar, index) => (
                <PendaftarItem 
                  key={index} 
                  pendaftar={pendaftar} 
                  onApprove={this.handleApprove}
                  onDecline={this.handleDecline}
                  onChange={this.handleChange}
                />
              ))}
              
              <Pagination 
                active={store.pendaftarListMeta.page}
                perPage={store.pendaftarListMeta.per_page}
                totalData={store.pendaftarListMeta.total_data}
                onChange={this.handleChangePage}
              />
            </div>
          : 
            <div>Pendaftar Tidak Ditemukan...</div>
        }
      </div>
    );
  }
}

export default hot(module)(observer(PendaftarListPage));