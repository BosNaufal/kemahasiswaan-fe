import * as React from 'react';
import styled from 'react-emotion'
import { Button } from 'src/components/Styled/General';
import { Pendaftar } from 'src/pages/Beasiswa/Detail/store';

export interface ActionButtonProps {
  loading?: boolean
  pendaftar: Pendaftar
  onChange(pendaftar: Pendaftar): void
  onApprove(pendaftar: Pendaftar): void
  onDecline(pendaftar: Pendaftar): void
}
 
const ActionButton: React.SFC<ActionButtonProps> = ({
  loading = false,
  pendaftar,
  onChange,
  onApprove,
  onDecline,
}: ActionButtonProps) => {
  const handleChange = () => {
    onChange(pendaftar)
  }

  const handleApprove = () => {
    onApprove(pendaftar)
  }

  const handleDecline = () => {
    onDecline(pendaftar)
  }

  return ( 
    !loading ? 
      <ButtonWrapper>
        {pendaftar.status &&
          <Button onClick={handleChange}>
            <i className="ion ion-md-create" />
            <span>CHANGE</span>
          </Button>
        }
        {!pendaftar.status &&
          <Button onClick={handleApprove}>
            <i className="ion ion-md-checkmark" />
            <span>APPROVE</span>
          </Button>
        }
        {!pendaftar.status &&
          <Button danger={true} onClick={handleDecline}>
            <i className="ion ion-md-close" />
            <span>REJECT</span>
          </Button>
        }
      </ButtonWrapper>
    :
      <ButtonWrapper>
        <div>Loading...</div>
      </ButtonWrapper>
  );
}

const ButtonWrapper = styled('div')`
  display: flex;
  justify-content: flex-end;
  flex-wrap: wrap;
`
 
export default ActionButton;