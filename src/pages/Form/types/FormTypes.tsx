export type Answer = {
  text: string
  checked: boolean
} 

export type FormModelProps = {
  type: string
  question: string
  answers: Answer[]
  required: boolean
}
