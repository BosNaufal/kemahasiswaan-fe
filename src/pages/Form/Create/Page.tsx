import * as React from 'react';
import CreateForm from './components/CreateForm';
import { hot } from 'react-hot-loader'

// export interface IFormPageProps {
  
// }
 
const FormPage: React.SFC<{}> = () => {
  return ( 
    <div>
      <CreateForm />
    </div>
  );
}
 
export default hot(module)(FormPage);