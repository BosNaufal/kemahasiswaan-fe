import { observable, action, computed } from 'mobx'
import FormModel from './FormCard/Model'

class CreateFormModel {
  @observable public forms: FormModel[] = []

  @computed get validForms() {
    const validForms = this.forms.map((form) => ({
      ...form,
      answers: form.answers.filter((answer) => answer.text)
    }))
    return validForms
  }

  @action public setForms = (payload: FormModel[]): void => {
    this.forms = payload
  }

  @action public addForm = (form: FormModel): void => {
    this.forms = [...this.forms, form]
  }

  @action public addFormAt = (index: number, form: FormModel): void => {
    const newFormModels: FormModel[] = [...this.forms]
    newFormModels.splice(index, 0, form)
    this.forms = newFormModels
  }
  
  @action public updateFormAt = (index: number, formUpdate: FormModel): void => {
    const newForms: FormModel[] = [...this.forms]
    newForms[index] = formUpdate
    this.forms = newForms
  }

  @action public deleteFormAt = (index: number): void => {
    const newFormModels: FormModel[] = [...this.forms]
    newFormModels.splice(index, 1)
    this.forms = newFormModels
  }
}

export default CreateFormModel
