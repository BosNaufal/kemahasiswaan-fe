import * as React from 'react';
import * as Enzyme from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';

import FormCard from './index';

Enzyme.configure({ adapter: new Adapter() });

describe("<FormCard />", () => {
  let props: any
  let renderedComponent: Enzyme.ReactWrapper | undefined
  const renderComponent = (): Enzyme.ReactWrapper => {
    if (!renderedComponent) {
      renderedComponent = Enzyme.mount(<FormCard {...props} />);
    }
    return renderedComponent
  }

  beforeEach(() => {
    props = {}
    renderedComponent = undefined
  })

  it('renders without crashing', () => {
    renderComponent()
    // Enzyme.mount(<FormCard />);
  });
})
