import { observable, action } from 'mobx'
import { uuid } from 'lil-uuid'
import { Answer, FormModelProps } from '../../../types/FormTypes'

class FormModel {
  public id: string = ""
  @observable public type: string = "text"
  @observable public question: string = ""
  @observable public answers: Answer[] = []
  @observable public required: boolean = false

  constructor() {
    this.id = uuid()
  }

  @action public fromObject = (payload: Partial<FormModelProps>): FormModel => {
    Object.keys(payload).forEach((key) => {
      this[key] = payload[key]
    })
    return this
  }

  @action public setType(payload: string): void {
    if (payload === "radio" || payload === "checkbox") {
      this.setAllAnswersCheckedTo(false)
      if (!this.answers.length) {
        this.addAnswer({ text: "", checked: false })
      }
    }
    this.type = payload
  }

  @action public setQuestion = (payload: string): void => {
    this.question = payload
  }

  @action public setRequired = (payload: boolean): void => {
    this.required = payload
  }

  @action public setAnswers = (payload: Answer[]): void => {
    this.answers = payload
  }

  @action public addAnswer = (answer: Answer): void => {
    this.answers = [...this.answers, answer]
  }

  @action public addAnswerAt = (index: number, answer: Answer): void => {
    const newAnswers: Answer[] = [...this.answers]
    newAnswers.splice(index, 0, answer)
    this.answers = newAnswers
  }

  @action public radioCheckAt = (index: number): void => {
    const newAnswers: Answer[] = this.answers.map((answer: Answer, i: number): Answer => {
      if (i === index) {
        return { ...answer, checked: true }
      }
      return { ...answer, checked: false }
    })
    this.answers = newAnswers
  }

  @action public updateAnswerAt = (index: number, answerUpdate: Partial<Answer>): void => {
    const answerTarget: Answer = this.answers[index]
    const newAnswers: Answer[] = [...this.answers]
    newAnswers[index] = {...answerTarget, ...answerUpdate }
    this.answers = newAnswers
  }

  @action public deleteAnswerAt = (index: number): void => {
    const newAnswers: Answer[] = [...this.answers]
    newAnswers.splice(index, 1)
    this.answers = newAnswers
  }


  @action private setAllAnswersCheckedTo = (payload: boolean = false) => {
    const newAnswers: Answer[] = this.answers.map((answer) => ({
      ...answer,
      checked: payload,
    }))
    this.answers = newAnswers
  }
}

export default FormModel