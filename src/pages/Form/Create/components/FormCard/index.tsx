import * as React from 'react';
import styled from 'react-emotion'
import { observer } from 'mobx-react'

import { Input, Select } from 'src/components/Styled/Form'

import FormModel from './Model'
import ActionFooter from './ActionFooter';
import AnswerManager from './AnswerManager';

export interface IFormCardProps {
  index: number
  model: FormModel
}
 
// export interface FormCardState {
  
// }

class FormCard extends React.Component<IFormCardProps, {}> {
  public model: FormModel

  public questionTypes = [
    { value: "text", text: "Text" },
    { value: "textarea", text: "Textarea" },
    { value: "number", text: "Number" },
    { value: "checkbox", text: "Checkbox" },
    { value: "radio", text: "Radio" },
    { value: "file", text: "File" },
  ]

  constructor(props: IFormCardProps) {
    super(props)
  }

  public handleQuestionChange = (e: React.FormEvent) => {
    const el = e.target as HTMLInputElement
    this.props.model.setQuestion(el.value)
  }

  public handleTypeChange = (e: React.FormEvent) => {
    const el = e.target as HTMLInputElement
    this.props.model.setType(el.value)
  }

  public render() { 
    const { model } = this.props
    return (
      <Wrapper>
        <Body>
          <LeftSide>
            <Handle className="drag-handler">:::</Handle>
            <QuestionInput 
              type="text" 
              value={model.question} 
              onChange={this.handleQuestionChange} 
              placeholder="Type Your Question..." 
            />
            <Select onChange={this.handleTypeChange} value={model.type}>
              {this.questionTypes.map((type, index) => (
                <option key={index} value={type.value}>{type.text}</option>
              ))}
            </Select>
            {model.type === "checkbox" || model.type === "radio"?
              <AnswerManager model={model} />
            :null}
          </LeftSide>
        </Body>
        <ActionFooter 
          index={this.props.index} 
          model={model} 
        />
      </Wrapper>
    );
  }
}

const Handle = styled("div")`
  font-weight: 700;
  font-size: 1.7em;
  line-height: 0;
  text-align: center;
  cursor: move;
  width: 100%;
  margin-bottom: 10px;
  color: #AAA;
`
 
 
const Wrapper = styled('div')`
  background: white;
  border: 1px solid #DDD;
  /* border-bottom: 1px solid #f8f8f8; */
  /* box-shadow: 1px 3px 5px 0 rgba(0,0,0,.2); */
`

const LeftSide = styled('div')`
  flex: 1;
`

const QuestionInput = styled(Input)`
  margin-top: 10px;
  margin-bottom: 10px;
  font-size: 1.2em;
  padding: 5px;
  border: none;
  /* border-bottom: 1px solid #DDD; */
`

const Body = styled('div')`
  display: flex;
  padding: 17px 15px;
`


export default observer(FormCard);