import * as React from 'react';
import styled from 'react-emotion'
import { observer, inject } from 'mobx-react';

import { Checkbox } from 'src/components/Styled/Form'
import { Button } from 'src/components/Styled/General'

import FormModel from './Model';
import CreateFormModelClass from '../CreateFormModel'

export interface IActionFooterProps {
  index: number,
  model: FormModel
}

export interface IInjectedActionFooterProps {
  CreateFormModel?: CreateFormModelClass
}
 
class ActionFooter extends React.Component<IInjectedActionFooterProps & IActionFooterProps, {}> {
  public handleRequiredChange = (e: React.FormEvent) => {
    const el = e.target as HTMLInputElement
    this.props.model.setRequired(el.checked)
  }

  public handleAdd = () => {
    const CreateFormModel = this.props.CreateFormModel!
    CreateFormModel.addFormAt(this.props.index + 1, new FormModel())
  }

  public handleDelete = () => {
    const CreateFormModel = this.props.CreateFormModel!
    const { index } = this.props
    const isLastFormCard = CreateFormModel.forms.length === 1
    if (isLastFormCard) {
      return false
    }
    CreateFormModel.deleteFormAt(index)
    return true
  }

  public render() { 
    const { model } = this.props
    const CreateFormModel = this.props.CreateFormModel!
    const isLastFormCard = CreateFormModel.forms.length === 1
    return ( 
      <Foot>
        <div>
          <Checkbox 
            label="Required"
            HTMLProps={{
              type: "checkbox",
              checked: model.required,
              onChange: this.handleRequiredChange,
            }}
          />
        </div>
        <Button onClick={this.handleAdd}>
          <i className="ion ion-md-add" />
          <span>ADD</span>
        </Button>
        {!isLastFormCard?
          <Button danger={true} onClick={this.handleDelete}>
            <i className="ion ion-md-trash" />
            <span>DELETE</span>
          </Button>
        :null}
      </Foot>
    );
  }
}
 
const Foot = styled("div")`
  display: flex;
  justify-content: flex-end;
  padding: 7px 15px 20px;
  align-items: center;
`
export default inject("CreateFormModel")(observer(ActionFooter));