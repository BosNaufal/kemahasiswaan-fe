
import FormModel from './Model'

describe("FormModel", () => {
  const model = new FormModel()
  model.addAnswer({ text: "Answer 1", checked: false })
  model.addAnswer({ text: "Answer 3", checked: false })

  it("Add answer at index", () => {
    model.addAnswerAt(1, { text: "Answer 2", checked: false })
    expect(model.answers.length).toBe(3)
    expect(model.answers[1].text).toBe("Answer 2")
  })

  it("Delete answer at index", () => {
    model.deleteAnswerAt(1)
    expect(model.answers.length).toBe(2)
    expect(model.answers[1].text).toBe("Answer 3")
  })

  it("Update answer at index", () => {
    model.updateAnswerAt(1, { text: "Answer 2" })
    model.updateAnswerAt(1, { checked: true })
    expect(model.answers.length).toBe(2)
    expect(model.answers[1].text).toBe("Answer 2")
    expect(model.answers[1].checked).toBe(true)
  })
})