import * as React from 'react';
import styled from 'react-emotion'
import pure from 'recompose/pure'

import { Input, Checkbox } from 'src/components/Styled/Form'
import { Answer } from '../../../types/FormTypes'

const Handle = () => (
  <StyledHandle className="drag-handler">
    <i className="ion ion-md-menu" />
  </StyledHandle>
)

export interface IAnswerItemProps extends Answer {
  id: string,
  index: number,
  type: string,
  onChange(index: number, key: string, e: React.SyntheticEvent): void
  onFocus?(index: number, key: string, e: React.SyntheticEvent): void
  onDelete(index: number): void
}

const AnswerItem: React.ComponentType<IAnswerItemProps> = pure<IAnswerItemProps>(({
  id,
  type,
  index,
  checked,
  text,
  onChange,
  onFocus = () => {/**/},
  onDelete,
}: IAnswerItemProps) => (
  <Item>
    <Handle />
    <CheckboxStyled
      round={type === "radio"}
      HTMLProps={{
        type,
        checked,
        onChange: onChange.bind(null, index, "checked"),
        name: `options-${id}`,
      }}
    />
    <InputLine>
      <StyledInput 
        type="text" 
        value={text} 
        placeholder="Answer Text" 
        onChange={onChange.bind(null, index, "text")}
        onFocus={onFocus.bind(null, index, "text")}
      />
      <DeleteButton onClick={onDelete.bind(null, index)}>
        <span>
          <i className="ion-md-close" />
        </span>
      </DeleteButton>
    </InputLine>
  </Item>
))

const StyledHandle = styled("div")`
  padding: 5px 7px;
  line-height: 0;
  cursor: move;
  font-size: 1.5em;
  margin-right: 5px;
`

const Item = styled("div")`
  display: flex;
  align-items: center;
  margin-bottom: 7px;
  align-items: center;
`

const CheckboxStyled = styled(Checkbox)`
  font-size: 1.2em;
`

const InputLine = styled("div")`
  display: flex;
  width: 100%;
`

const StyledInput = styled(Input)`
  flex: 1;
`

const DeleteButton = styled("button")`
  padding: 2px 10px;
  font-size: 1.1em;
  cursor: pointer;
  color: var(--red);
  border: 0;
  outline: none;
  background: none;

  span {
    display: inline-block;
    background: var(--red);
    color: white;
    line-height: 0;
    width: 21px;
    height: 21px;
    border-radius: 50%;
    display: flex;
    align-items: center;
    justify-content: center;
  }
`

export default AnswerItem