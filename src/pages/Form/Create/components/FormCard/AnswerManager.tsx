import * as React from 'react';
import { observer } from 'mobx-react'
import FormModel from './Model';
import styled from 'react-emotion'

import { Answer } from '../../../types/FormTypes'

import AnswerItem from './AnswerItem'
import Dragula from 'src/components/Dragula/index'

export interface IAnswerManagerProps {
  model: FormModel
}
 
class AnswerManager extends React.Component<IAnswerManagerProps, {}> {

  public handleAnswerUpdate = (index: number, answerUpdate: Partial<Answer>) => {
    this.props.model.updateAnswerAt(index, answerUpdate)
  }
  
  public handleAddAnswer = () => {
    this.props.model.addAnswer({ text: "", checked: false })
  }

  public autoAddAnswer = (index: number, value: string) => {
    const { model } = this.props
    const isLastAnswer = model.answers.length - 1 === index
    if (isLastAnswer && value) {
      model.addAnswer({ text: "", checked: false })
    }
  }

  public handleAnswerChangeAt = (index: number, key: string, e: React.SyntheticEvent): void => {
    const { model } = this.props
    const el = e.target as HTMLInputElement
    let validValue: string | boolean = el.value

    if (key === "checked") {
      validValue = el.checked
      if (model.type === "radio") {
        model.radioCheckAt(index)
      }
    } else {
      this.autoAddAnswer(index, validValue)
    }

    const answerUpdate: Partial<Answer> = {
      [key]: validValue
    }
    model.updateAnswerAt(index, answerUpdate)
  }

  public handleDeleteAt = (index: number): boolean => {
    const { model } = this.props
    const isLastAnswer: boolean = index === model.answers.length - 1
    const answerText: string = model.answers[index].text
    const isPlaceholder: boolean = isLastAnswer && !answerText
    const isTheOnlyOne: boolean = index === 0 && model.answers.length === 1
    if (isPlaceholder || isTheOnlyOne) {
      return false
    }
    this.props.model.deleteAnswerAt(index)
    return true
  }

  public render() { 
    const { model } = this.props
    return (
      <div>
        <Container>
          <Dragula 
            items={model.answers} 
            handle="drag-handler"
            onSort={model.setAnswers}
          >
            {(answer: Answer, index: number) => (
              <div data-index={index} key={index}>
                <AnswerItem 
                  id={model.id}
                  type={model.type}
                  index={index}
                  {...answer}
                  onFocus={this.handleAnswerChangeAt}
                  onChange={this.handleAnswerChangeAt}
                  onDelete={this.handleDeleteAt}
                />
              </div>
            )}
          </Dragula>
        </Container>
      </div>
    );
  }
}

const Container = styled("div")`
  margin-top: 15px;
  margin-bottom: 15px;
`
export default observer(AnswerManager);