import * as React from 'react';
import styled from 'react-emotion'
import { Provider, observer } from 'mobx-react'
// import { toJS } from 'mobx'

import Dragula from 'src/components/Dragula/index'
// import { Button } from 'src/components/Styled/General'

import FormCard from './FormCard/index';
import CreateFormModel from './CreateFormModel'
import FormModel from './FormCard/Model'

// import * as styled from 'react-emotion'

export interface CreateFormProps {
  defaultValue?: string
}
 
class CreateForm extends React.Component<CreateFormProps, {}> {

  public model: CreateFormModel

  constructor(props: CreateFormProps) {
    super(props)
    this.model = new CreateFormModel()
    if (props.defaultValue) {
      const forms = JSON.parse(props.defaultValue)
      const formModels = forms.map((form: object) => (new FormModel()).fromObject(form))
      this.model.setForms(formModels)
    } else {
      this.model.addForm(new FormModel())
    }
  }

  public render() { 
    const { model } = this
    return (
      <Provider CreateFormModel={this.model}>
        <div className="mini-container">
          <StyledFormList 
            items={model.forms} 
            handle="drag-handler"
            onSort={model.setForms}
          >
            {(item: FormModel, index: number) => (
              <FormCardItemWrapper data-index={index} key={index}>
                <FormCard
                  index={index} 
                  model={item}
                />
              </FormCardItemWrapper>
            )}
          </StyledFormList>
        </div>
      </Provider>
    );
  }
}

const StyledFormList = styled(Dragula)`
  margin-top: 15px;
  border-radius: 7px;
  overflow: hidden;
  margin-bottom: 45px;
  /* box-shadow: 2px 3px 5px 0 rgba(0,0,0,.3); */
  /* border: 1px solid #EEE; */
`

const FormCardItemWrapper = styled('div')`
  background: white;
`

export {
  CreateForm
}
export default observer(CreateForm);