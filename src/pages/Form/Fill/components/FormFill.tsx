import * as React from 'react';
import styled from 'react-emotion'

import FormFillModel from './FormFillModel'
import FieldModel from './FieldForm/FieldModel'

import FieldForm from './FieldForm/index'
import { FormModelProps } from '../../types/FormTypes';
import FormModel from '../../Create/components/FormCard/Model';
import { Button } from 'src/components/Styled/General';

interface FormFillProps {
  readOnly?: boolean
  isSubmitting?: boolean
  form: any
  onSubmit?: (formModel: FormFillModel) => void
}

class FormFill extends React.Component<FormFillProps, {}> {
  public model: FormFillModel

  public constructor(props: any) {
    super(props);
    const formObject = typeof props.form === 'string' 
      ? JSON.parse(props.form)
      : props.form.map((form: any) => ({
        ...form,
        answers: JSON.parse(form.answers),
      }))
    const forms: FormModelProps[] = formObject
      .map((form: object) => new FormModel().fromObject(form))
    this.model = new FormFillModel(forms)
  }

  handleSubmit = () => {
    if (this.props.onSubmit) {
      this.props.onSubmit(this.model)
    }
  }

  public render() { 
    const { model } = this
    const {  
      readOnly = false,
      isSubmitting = false,
    } = this.props
    return ( 
      <div className="mini-container">
        <FieldList>
          {model.fields.map((field: FieldModel, index: number) => (
            <FieldForm readOnly={readOnly} field={field} key={index} />
          ))}
          {this.props.onSubmit &&
            <ButtonWrapper>
              <Button onClick={this.handleSubmit} disabled={isSubmitting}>
                {!isSubmitting &&
                  <i className="ion ion-md-send" />
                }
                <span>{isSubmitting ? "SUBMITTING..." : "SUBMIT"}</span>
              </Button>
            </ButtonWrapper>
          }
        </FieldList>
      </div>
    );
  }
}

const FieldList = styled('div')`
  padding: 15px 20px;
  background: white;
  border-radius: 5px;
  overflow: hidden;
  border: 1px solid #DDD;
  margin: 10px 0 25px;
`

const ButtonWrapper = styled('div')`
  margin-top: 25px;
  display: flex;
  justify-content: flex-end;
`
 
export default FormFill;