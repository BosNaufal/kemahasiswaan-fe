import { observable, action, computed } from 'mobx'
import FieldModel from './FieldForm/FieldModel'
import { FormModelProps } from '../../types/FormTypes'

class FormFillModel {
  @observable public fields: FieldModel[] = []

  constructor(fields: FormModelProps[]) {
    this.setFields(fields.map((field) => (new FieldModel).fromObject(field)))
  }

  @action public setFields = (payload: FieldModel[]): void => {
    this.fields = payload
  }

  @computed get hasFilledRequiredFields() {
    return this.fields.filter((field) => (
      field.required && !field.value
    )).length === 0
  }

  @computed get validForms() {
    return this.fields.map((field) => ({
      ...field,
      question_id: field.questionId,
      value: field.value,
      checkedValue: field.checkedValue,
    }))
  }

}

export default FormFillModel
