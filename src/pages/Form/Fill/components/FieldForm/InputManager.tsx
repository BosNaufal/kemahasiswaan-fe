import * as React from 'react';
import { observer } from 'mobx-react'
import styled from 'react-emotion'
import { uuid } from 'lil-uuid'

import FieldModel from './FieldModel'
import { Checkbox } from 'src/components/Styled/Form'

export interface IInputManagerProps {
  field: FieldModel
  readOnly?: boolean
}
 
class InputManager extends React.Component<IInputManagerProps, {}> {
  public id: string

  constructor(props: IInputManagerProps) {
    super(props);
    this.id = uuid()
  }

  public handleCheck = (index: number) => {
    const { field, readOnly } = this.props
    if (!readOnly) {
      if (field.type === "radio") {
        field.radioCheckAt(index)
      } else {
        field.toggleCheckboxAt(index)
      }
    }
  }

  public render() { 
    const { field } = this.props
    return (
      <Outer>
        {field.answers.map((answer, index) => (
          <CheckboxWrapper key={index}>
            <Checkbox
              round={field.type === "radio"}
              HTMLProps={{
                type: field.type,
                checked: answer.checked,
                onChange: this.handleCheck.bind(null, index),
                name: `options-${this.id}`,
              }}
            />
            <div>{answer.text}</div>
          </CheckboxWrapper>
        ))}
      </Outer>
    );
  }
}

const Outer = styled('div')`
  margin-bottom: 20px;
`

const CheckboxWrapper = styled("div")`
  display: flex;
  align-items: center;
  margin-top: 10px;
  margin-bottom: 7px;
`
 
export default observer(InputManager);