import * as React from 'react';


import { Input } from 'src/components/Styled/Form'
import FieldModel from './FieldModel';
import Req, { fileURL } from 'src/api';

export interface InputFileProps {
  field: FieldModel
  readOnly?: boolean
}
 
export interface InputFileState {
  loading: boolean
  progress: number
}
 
class InputFile extends React.Component<InputFileProps, InputFileState> {
  state = { 
    loading: false,
    progress: 0  
  }

  handleChange = async (e: React.SyntheticEvent) => {
    if (!this.props.readOnly) {
      const el = e.target as HTMLInputElement
      const { files } = el
      const form = new FormData()
      form.append('file', files![0])
  
      this.setState({ loading: true })
  
      const res = await Req({
        method: 'POST',
        url: '/file', 
        data: form,
        onUploadProgress: (evt: ProgressEvent) => {
          const progress = evt.loaded / evt.total * 100
          this.setState({ progress })
        }
      })
      const { data } = res
      const { filename } = data
      this.props.field.setTextAnswer(filename)
  
      this.setState({ loading: false })
    }
  }

  render() { 
    const { field, readOnly } = this.props
    const { progress } = this.state
    return (
      <div>
        {progress > 0 && progress < 100 &&
          <div>Uploading {progress}%</div>
        }
        {(progress === 100 || readOnly) &&
          <div>
            <span>Uploaded File </span> 
            <a href={`${fileURL}/${field.value}`} target="_blank">{field.value}</a>
          </div>
        }
        {!readOnly &&
          <Input 
            type="file" 
            onChange={this.handleChange} 
            multiple={false}
          />
        }
      </div>
    );
  }
}
 
export default InputFile;