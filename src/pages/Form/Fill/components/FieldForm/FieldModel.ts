import { observable, action, computed } from 'mobx'
import { Answer, FormModelProps } from '../../../types/FormTypes'

class FieldModel {
  @observable public id: string = ""
  @observable public type: string = "text"
  @observable public question: string = ""
  @observable public answers: Answer[] = []
  @observable public textAnswer: string = ""
  @observable public required: boolean = false

  @computed get isCheckInput() {
    const isCheckInput = 
      this.type === "checkbox" || this.type === "radio"
    return isCheckInput
  }

  @computed get questionId() {
    return this.id || null
  }

  @computed get checkedValue() {
    let value: any = false
    if (this.isCheckInput) {
      const checkedAnswers = this.answers.filter((answer) => answer.checked)
      if (checkedAnswers.length) {
        value = checkedAnswers
      } else {
        value = false
      }
    }
    return value
  }

  @computed get value() {
    let value = this.textAnswer
    if (this.isCheckInput) {
      const checkedAnswers = this.answers.filter((answer) => answer.checked)
      if (checkedAnswers.length) {
        value = JSON.stringify(checkedAnswers.map((answer) => answer.text))
      } else {
        value = ""
      }
    }
    return value
  }

  @action public fromObject = (payload: Partial<FormModelProps>): FieldModel => {
    Object.keys(payload).forEach((key) => {
      if (key === 'value') {
        this.textAnswer = payload[key]
      } else {
        this[key] = payload[key]
      }
    })
    return this
  }

  @action public setTextAnswer = (textAnswer: string) => {
    this.textAnswer = textAnswer
  }

  @action public radioCheckAt = (index: number): void => {
    const newAnswers: Answer[] = this.answers.map((answer: Answer, i: number): Answer => {
      if (i === index) {
        return { ...answer, checked: true }
      }
      return { ...answer, checked: false }
    })
    this.answers = newAnswers
  }

  @action public toggleCheckboxAt = (index: number): void => {
    const answerTarget: Answer = this.answers[index]
    this.updateAnswerAt(index, { ...answerTarget, checked: !answerTarget.checked })
  }

  @action public updateAnswerAt = (index: number, answerUpdate: Partial<Answer>): void => {
    const answerTarget: Answer = this.answers[index]
    const newAnswers: Answer[] = [...this.answers]
    newAnswers[index] = {...answerTarget, ...answerUpdate }
    this.answers = newAnswers
  }

  @action public setAllAnswersCheckedTo = (payload: boolean = false): void => {
    const newAnswers: Answer[] = this.answers.map((answer) => ({
      ...answer,
      checked: payload,
    }))
    this.answers = newAnswers
  }
}

export default FieldModel