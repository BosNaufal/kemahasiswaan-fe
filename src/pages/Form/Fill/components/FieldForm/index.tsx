import * as React from 'react';
import styled from 'react-emotion'
import { observer } from 'mobx-react';

import FieldModel from './FieldModel'

import { Input, Textarea } from 'src/components/Styled/Form'
import InputManager from './InputManager'
import InputFile from './InputFile';

export interface IFieldFormProps {
  field: FieldModel
  readOnly: boolean
}
 
class FieldForm extends React.Component<IFieldFormProps, {}> {

  public handleTextAnswer = (e: React.SyntheticEvent) => {
    if (!this.props.readOnly) {
      const el = e.target as HTMLInputElement
      this.props.field.setTextAnswer(el.value)
    }
  }

  public renderInput = () => {
    const { field, readOnly } = this.props
    const { type } = field
    const isCheckInput = type === "checkbox" || type === "radio"
    if (isCheckInput) {
      return <InputManager readOnly={readOnly} field={field} />
    }
    switch(type) {
      case "textarea": {
        return <Textarea value={field.value} onChange={this.handleTextAnswer} />
      }
      case "file": {
        return <InputFile readOnly={readOnly} field={field} />
      }
      default: {
        return <Input type={type} value={field.value} onChange={this.handleTextAnswer} />
      }
    }    
  }

  public render() {
    const { field } = this.props
    const isValid = field.required 
      ? field.value.length > 0
      : true
    return ( 
      <Wrapper>
        <Question valid={isValid}>
          {field.question} {!!field.required && '*'}
        </Question>
        {this.renderInput()}
      </Wrapper>
    );
  }
}

const Wrapper = styled('div')`
  margin-top: 20px;
  background: white;
`

const Question = styled('div')`
  font-size: 1.2em;
  margin-bottom: 7px;

  ${({ valid }: any) => !valid && ({
    color: 'var(--red)'
  })}
`
 
export default observer(FieldForm);