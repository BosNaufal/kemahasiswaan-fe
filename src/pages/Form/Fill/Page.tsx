import * as React from 'react';
import { hot } from 'react-hot-loader'

import FormFill from './components/FormFill';
import FormFillModel from './components/FormFillModel';

// export interface IFormFillPageProps {
  
// }
 
const FormFillPage: React.SFC<{}> = () => {
  const handleSubmit = (model: FormFillModel) => {
    //
  }
  return ( 
    <div>
      <FormFill form="[]" onSubmit={handleSubmit} />
    </div>
  );
}
 
export default hot(module)(FormFillPage);