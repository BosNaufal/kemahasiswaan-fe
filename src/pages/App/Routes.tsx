import * as React from 'react';
import { Route, RouteProps } from 'react-router-dom'
import { observer } from 'mobx-react';

import CreateFormPage from 'src/pages/Form/Create/index'
import FillFormPage from 'src/pages/Form/Fill/index'
import BeasiswaPageDetail from 'src/pages/Beasiswa/Detail/index'
import BeasiswaListPage from 'src/pages/Beasiswa/List/index'
import LoginPage from 'src/pages/Login'

import PendaftarList from 'src/pages/Pendaftar/List/index'
import PendaftarDetailPage from 'src/pages/Pendaftar/Detail/index'

import UmumkanFormPage from 'src/pages/Umumkan/Form/index'
import AnalyticListPage from 'src/pages/Analytic/List/index'

import store from './store';

const AdminOnly = (Component: React.ComponentType): React.SFC<RouteProps> => observer((props) => {
  if (store.isLogged) {
    return ( 
      <Component {...props} />
    )
  }
  window.location.hash = "#/login"
  return null
})

const isNotLogged = (Component: React.ComponentType): React.SFC<RouteProps> => observer((props) => {
  if (!store.isLogged) {
    return ( 
      <Component {...props} />
    )
  }
  window.location.hash = "#/"
  return null
})

class Routes extends React.Component<{}, {}> {
  public render() { 
    return (
      <div>
        <Route exact={true} path="/" component={BeasiswaListPage} />
        <Route exact={true} path="/login" component={isNotLogged(LoginPage)} />

        <Route exact={true} path="/form/create" component={AdminOnly(CreateFormPage)} />
        <Route exact={true} path="/form/fill" component={FillFormPage} />

        <Route exact={true} path="/beasiswa/create" component={AdminOnly(BeasiswaPageDetail)} />
        <Route exact={true} path="/beasiswa/detail/:beasiswaId" component={BeasiswaPageDetail} />

        <Route exact={true} path="/pendaftar/beasiswa/:beasiswaId" component={PendaftarList} />
        <Route exact={true} path="/pendaftar/:pendaftarId" component={AdminOnly(PendaftarDetailPage)} />

        <Route exact={true} path="/umumkan/beasiswa/:beasiswaId" component={AdminOnly(UmumkanFormPage)} />

        <Route exact={true} path="/analytic" component={AdminOnly(AnalyticListPage)} />
      </div>
    );
  }
}
 
export default Routes;