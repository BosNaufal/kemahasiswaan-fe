import { observable, action, computed } from 'mobx'
import Cookies from 'js-cookie'

import Req, { updateToken } from 'src/api'

export class Global {
  @observable userInfo: any = {}

  @computed get isLogged() {
    const isLogged = store.userInfo && store.userInfo.id
    return isLogged 
  }

  @action setUserInfo = (payload: any) => {
    this.userInfo = payload
  }

  @action checkAuth = async () => {
    const token = Cookies.get("token")
    if (token) {
      updateToken(token)
      const res = await Req.get("/auth", {
        params: {
          token,
        },
      })
      if (res.error) {
        this.logout()
        return false
      }
      const { data } = res
      this.setUserInfo(data);
      return res
    }
    return false
  }

  @action logout = () => {
    this.setUserInfo({})
    Cookies.remove('token')
  }
}

const store = new Global()
export default store


