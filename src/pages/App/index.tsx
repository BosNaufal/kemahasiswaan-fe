import * as React from 'react';
import { hot } from 'react-hot-loader'
import { injectGlobal } from 'emotion'
import 'pretty-checkbox/dist/pretty-checkbox.css'
import Routes from './Routes'
import Navbar from 'src/components/Navbar/index';
import store from './store'

class App extends React.Component<{}, any> {
  state = {
    ready: false,
  }

  async componentDidMount() {
    await store.checkAuth()
    this.setState({ ready: true })
  }

  public render() {
    return this.state.ready && (
      <div>
        <div className="MainLayout">
          <Navbar />
          <div className="MainContent">
            <div className="MainContentScroller">
              <Routes />
            </div>
          </div>
        </div>
      </div>
    );
  }
}

// tslint:disable-next-line
injectGlobal`
  * {
    box-sizing: border-box;
  }

  body {
    /* --body-background: #f3fff6; */
    /* --body-background: #f8fffa; */
    /* --body-background: #f6f9f7; */
    /* --primary: #00cc75; */
    /* --primary: #3bb16f; */
    /* --primary: #43A047; */
    /* --primary-light: #E8F5E9; */

    /* --primary: #0288d1; */
    --primary: #009688;
    /* --primary: #26A65B; */

    /* --primary: #299c6b; */
    --secondary: #d2f7dc;
    --darker: #299c6b;
    --white: #fff;
    --black: #2b2b2b;
    --orange: #f1c40f;
    --red: #E53935;
    --red-surface: #FFCDD2;

    /* font-family: "Lato", sans-serif; */
    font-family: "Roboto", sans-serif;

    font-size: 0.95em;
    line-height: 1.4;
    /* background: #f9f9f9; */
    /* background: #fffaf7; */

    position: relative;
    width: 100%;
    height: 100vh;
    /* background: var(--body-background); */
  }

  #root {
    overflow: hidden;
  }

  img {
    width: 100%;
  }

  .fluid, #root {
    position: relative;
    width: 100%;
    height: 100%;
  }

  .MainLayout {
    display: flex;
    flex-direction: column;
    height: 100vh;
  }

  .MainContent {
    flex: 1;
    display: flex;
    overflow: auto;
    padding-top: 20px;
    padding-bottom: 35px;
    position: relative;
  }

  .MainContentScroller {
    min-height: min-content;
    width: 100%;
  }

  .MainWrapper {
    overflow-x: hidden;
    padding-top: 60px;
    padding-bottom: 100px;
  }

  .light-drop-shadow {
    filter: drop-shadow(2px 2px 2px var(--black));
  }

  .mini-container {
    max-width: 760px;
    padding: 0 10px;
    display: block;
    margin: 0 auto;
  }

  .text-center {
    text-align: center;
  }

`

export default hot(module)(App)
